/*
 *
 * Company :      IsuCorp
 * Project :      Reservations   
 * Author :       Diuber Estanque Diaz
 *
 
 */

 -----Store procedure to update reservations out of date to close -------
 CREATE PROCEDURE UpdateReservationStatus
AS
	UPDATE [schema].[Reservation]
	SET [StatusId] = (SELECT [ReservationStatusId] from [schema].[ReservationStatus] WHERE [Code] ='CLO')
	WHERE CAST([Date] as date) < CAST(GETDATE() as date) 
GO

		   
-------------------Contacts------------------------------------
IF NOT EXISTS (SELECT 1 FROM [schema].[Contact] where [Name] = 'Contact1' )
BEGIN
INSERT INTO [schema].[Contact]
           ([Name]
           ,[Phone]
					 ,[Rating]
					 ,[TypeId]
					 ,[BirthDate])
     VALUES
           ('Contact1'
					  ,'77768556'
						,1
						,(Select [ContactTypeId] from [schema].[ContactType] where [Code] = 'Type1')
						,GETDATE()
						
					 )

END

IF NOT EXISTS (SELECT 1 FROM [schema].[Contact] where [Name] = 'Contact2' )
BEGIN
INSERT INTO [schema].[Contact]
           ([Name]
           ,[Phone]
					 ,[Rating]
					 ,[TypeId]
					 ,[BirthDate])
     VALUES
           ('Contact2'
					  ,'777688594'
						,1
						,(Select [ContactTypeId] from [schema].[ContactType] where [Code] = 'Type2')
						,GETDATE()
						
					 )

END

IF NOT EXISTS (SELECT 1 FROM [schema].[Contact] where [Name] = 'Contact3' )
BEGIN
INSERT INTO [schema].[Contact]
           ([Name]
           ,[Phone]
					 ,[Rating]
					 ,[TypeId]
					 ,[BirthDate])
     VALUES
           ('Contact3'
					  ,'777688244'
						,1
						,(Select [ContactTypeId] from [schema].[ContactType] where [Code] = 'Type3')
						,GETDATE()
						
					 )

END

IF NOT EXISTS (SELECT 1 FROM [schema].[Contact] where [Name] = 'Contact4' )
BEGIN
INSERT INTO [schema].[Contact]
           ([Name]
           ,[Phone]
					 ,[Rating]
					 ,[TypeId]
					 ,[BirthDate])
     VALUES
           ('Contact4'
					  ,'854688244'
						,1
						,(Select [ContactTypeId] from [schema].[ContactType] where [Code] = 'Type4')
						,GETDATE()
						
					 )

END

------------------Reservations------------------------------------
IF NOT EXISTS (SELECT 1 FROM [schema].[Reservation] where [Description] = 'Reservation1' )
BEGIN
INSERT INTO [schema].[Reservation]
           ([ContactId]
           ,[Date]
					 ,[Description]
					 ,[Ranking]
					 ,[StatusId])
     VALUES
           ((Select [ContactId] from [schema].[Contact] where [Name] = 'Contact1')
					  ,GETDATE()
						,'Reservation1'
						,1
						,(Select [ReservationStatusId] from [schema].[ReservationStatus] where [Code] = 'rdy')						
					 )

END
IF NOT EXISTS (SELECT 1 FROM [schema].[Reservation] where [Description] = 'Reservation2' )
BEGIN
INSERT INTO [schema].[Reservation]
           ([ContactId]
           ,[Date]
					 ,[Description]
					 ,[Ranking]
					 ,[StatusId])
     VALUES
           ((Select [ContactId] from [schema].[Contact] where [Name] = 'Contact2')
					  ,GETDATE()
						,'Reservation2'
						,1
						,(Select [ReservationStatusId] from [schema].[ReservationStatus] where [Code] = 'rdy')						
					 )

END
IF NOT EXISTS (SELECT 1 FROM [schema].[Reservation] where [Description] = 'Reservation3' )
BEGIN
INSERT INTO [schema].[Reservation]
           ([ContactId]
           ,[Date]
					 ,[Description]
					 ,[Ranking]
					 ,[StatusId])
     VALUES
           ((Select [ContactId] from [schema].[Contact] where [Name] = 'Contact3')
					  ,GETDATE()
						,'Reservation3'
						,1
						,(Select [ReservationStatusId] from [schema].[ReservationStatus] where [Code] = 'rdy')						
					 )

END