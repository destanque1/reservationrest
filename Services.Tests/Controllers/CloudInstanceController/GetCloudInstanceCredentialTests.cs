﻿using System.Diagnostics.CodeAnalysis;
using Application.Dtos;
using Application.Dtos.Adapters;
using Application.IServices;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Services.Tests.Controllers.CloudInstanceController
{
    [ExcludeFromCodeCoverage]
    public class GetCloudInstanceCredentialTests
    {
        private Services.Controllers.CloudInstanceController _cloudInstanceController;

        private Services.Controllers.CloudInstanceController CloudInstanceController
        {
            get
            {
                if (_cloudInstanceController != null) return _cloudInstanceController;
                var mockService = new Mock<IInstanceApplicationService>();
                mockService.Setup(x => x.GetCloudInstanceCredential(1, 1))
                    .ReturnsAsync(CloudInstanceCredentialDtoAdapter.Create(new InstanceCredential()));
                _cloudInstanceController = new Services.Controllers.CloudInstanceController(mockService.Object);
                return _cloudInstanceController;
            }
        }

        [Fact]
        public async void GetCloudInstanceBy()
        {
            var result = await CloudInstanceController.GetCloudInstanceCredential(1, 1);
            Assert.IsAssignableFrom<ActionResult<CloudInstanceCredentialDto>>(result);
        }
    }
}
