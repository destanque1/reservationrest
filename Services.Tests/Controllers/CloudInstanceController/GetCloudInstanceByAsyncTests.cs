﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Application.Dtos;
using Application.Dtos.Adapters;
using Application.IServices;
using CrossCutting.Dtos;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Services.Tests.Controllers.CloudInstanceController
{
    [ExcludeFromCodeCoverage]
    public class GetCloudInstanceByAsyncTests
    {
        private Services.Controllers.CloudInstanceController _cloudInstanceController;

        private Services.Controllers.CloudInstanceController CloudInstanceController
        {
            get
            {
                if (_cloudInstanceController != null) return _cloudInstanceController;
                var mockService = new Mock<IInstanceApplicationService>();
                mockService.Setup(x => x.GetCloudInstanceByAsync(1, 1))
                    .ReturnsAsync(CloudInstanceDetailsDtoAdapter.Create(new Instance(), new Pzone(), new List<Disk>(), new List<ExternalIp>(), new List<InternalIp>()));
                _cloudInstanceController = new Services.Controllers.CloudInstanceController(mockService.Object);
                return _cloudInstanceController;
            }
        }

        [Fact]
        public async void GetCloudInstanceBy()
        {
            var result = await CloudInstanceController.GetCloudInstanceByAsync(1, 1);
        }
    }
}
