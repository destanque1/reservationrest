﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Application.Dtos;
using Application.IServices;
using CrossCutting.Adapters;
using CrossCutting.Dtos;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Services.Tests.Controllers.CloudInstanceController
{
    [ExcludeFromCodeCoverage]
    public class GetCloudInstancesAsyncTests
    {
        private Services.Controllers.CloudInstanceController _cloudInstanceController;

        private Services.Controllers.CloudInstanceController CloudInstanceController
        {
            get
            {
                if (_cloudInstanceController != null) return _cloudInstanceController;
                var mockService = new Mock<IInstanceApplicationService>();
                mockService.Setup(x => x.GetCloudInstancesAsync(4, 1, 1))
                    .ReturnsAsync(ListDtoAdapter<CloudInstanceDto>.Create(0, new List<CloudInstanceDto>()));
                _cloudInstanceController = new Services.Controllers.CloudInstanceController(mockService.Object);
                return _cloudInstanceController;
            }
        }


        [Fact]
        public async void ListInstacesTest()
        {

            var result = await CloudInstanceController.GetCloudInstancesAsync(4, 1, 1);
            Assert.IsAssignableFrom<ActionResult<ListDto<CloudInstanceDto>>>(result);
        }

    }
}