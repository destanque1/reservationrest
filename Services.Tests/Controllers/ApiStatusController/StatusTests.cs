﻿using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Services.Tests.Controllers.ApiStatusController
{
    [ExcludeFromCodeCoverage]
    public class StatusTests
    {
        private readonly Services.Controllers.ApiStatusController _controller;

        public StatusTests()
        {
            _controller = new Services.Controllers.ApiStatusController();
        }

        [Fact]
        public void Status()
        {
            var result = _controller.Status();

            Assert.Equal("Online", result);
        }
    }
}
