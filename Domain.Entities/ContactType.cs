﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public partial class ContactType
    {
        public ContactType()
        {
          
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ContactTypeId { get; set; }

        [Required]
        [StringLength(20)]
        public string Code { get; set; }
        [Required]
        public string Description { get; set; }
       
    }
}
