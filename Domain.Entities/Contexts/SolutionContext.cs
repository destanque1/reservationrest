﻿using Microsoft.EntityFrameworkCore;

namespace Domain.Entities.Contexts
{
    public partial class SolutionContext : DbContext
    {
        public SolutionContext(DbContextOptions<SolutionContext> options)
            : base(options)
        {
        }
      
        public virtual DbSet<Contact> Contact { get; set; }
        public virtual DbSet<ContactType> ContactType { get; set; }
        public virtual DbSet<Reservation> Reservation { get; set; }
        public virtual DbSet<ReservationStatus> ReservationStatus { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           
            modelBuilder.Entity<Contact>(entity =>
            {
                entity.ToTable("Contact", "schema");
            });

            modelBuilder.Entity<ContactType>(entity =>
            {
                entity.ToTable("ContactType", "schema");
            });

            modelBuilder.Entity<Reservation>(entity =>
            {
                entity.ToTable("Reservation", "schema");
            });

            modelBuilder.Entity<ReservationStatus>(entity =>
            {
                entity.ToTable("ReservationStatus", "schema");
            });

            //Encoder data
            modelBuilder.Entity<ContactType>().HasData(
                new ContactType { ContactTypeId = 1, Code = "Type1", Description = "Type1"},
                new ContactType { ContactTypeId = 2, Code = "Type2", Description = "Type2" },
                new ContactType { ContactTypeId = 3, Code = "Type3", Description = "Type3" },
                new ContactType { ContactTypeId = 4, Code = "Type4", Description = "Type4" });

            //Encoder data
            modelBuilder.Entity<ReservationStatus>().HasData(
                new ReservationStatus { ReservationStatusId = 1, Code = "PTC", Description = "Pending to create" },
                new ReservationStatus { ReservationStatusId = 2, Code = "PTU", Description = "Pending to update" },
                new ReservationStatus { ReservationStatusId = 3, Code = "RDY", Description = "Ready" },
                new ReservationStatus { ReservationStatusId = 4, Code = "CLO", Description = "Close" }
                );

           
        }
    }
}
