﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities.Abstracts
{
    /// <inheritdoc />
    /// <summary>
    /// ValueObject Base Class that encapsulates equality and identity boilerplate
    /// </summary>
    /// <typeparam name="T">The object</typeparam>
    public abstract class ValueObject<T> : IEquatable<T> where T : ValueObject<T>
    {
        protected abstract IEnumerable<object> GetAttributesToIncludeInEqualityCheck();

        public override bool Equals(object other)
        {
            return Equals(other as T);
        }

        public bool Equals(T other)
        {
            if (other == null)
            {
                return false;
            }
            return GetAttributesToIncludeInEqualityCheck()
                .SequenceEqual(other.GetAttributesToIncludeInEqualityCheck());
        }

        public static bool operator ==(ValueObject<T> left, ValueObject<T> right)
        {
            return left?.Equals(right) ?? Equals(right, null);
        }

        public static bool operator !=(ValueObject<T> left, ValueObject<T> right)
        {
            return !(left == right);
        }
        
    }
}
