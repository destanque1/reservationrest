﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public partial class ReservationStatus
    {
        public ReservationStatus()
        {
          
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ReservationStatusId { get; set; }

        [Required]
        [StringLength(20)]
        public string Code { get; set; }
        [Required]
        public string Description { get; set; }




    }
}
