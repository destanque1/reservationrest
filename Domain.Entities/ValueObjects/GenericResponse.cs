﻿using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities.ValueObjects
{
    public class GenericResponse<T> : BaseResponse
    {
        public T Value { get; set; }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            var result = base.GetAttributesToIncludeInEqualityCheck().ToList();
            result.Add(Value);

            return result;
        }
    }
}
