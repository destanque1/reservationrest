﻿namespace Domain.Entities.ValueObjects.Adapter
{
  
    public partial class ContactModelAdapter
    {
        /// <summary>
        /// Responsible for convert model to entity
        /// </summary>
        /// <param name="contactModel"></param>
        /// <returns>Returns Contact entity </returns>
        public static Contact Create(ContactModel contactModel)
        {
            if (contactModel == null) return null;
            return new Contact
            {
                ContactId = contactModel.ContactId,
                Name = contactModel.Name,
                Phone = contactModel.Phone,
                ContactType = new ContactType { ContactTypeId = contactModel.ContactTypeId },
                BirthDate = contactModel.BirthDate
            };
        }

        /// <summary>
        /// Responsible for convert entity to model
        /// </summary>
        /// <param name="contact"></param>
        /// <returns>Returns ContactModel entity </returns>
        public static ContactModel Create(Contact contact)
        {
            if (contact == null) return null;
            return new ContactModel
            {
                ContactId = contact.ContactId,
                Name = contact.Name,
                Phone = contact.Phone,
                ContactTypeId = contact.ContactType?.ContactTypeId ?? 0,
                BirthDate = contact.BirthDate
            };
        }


    }
}
