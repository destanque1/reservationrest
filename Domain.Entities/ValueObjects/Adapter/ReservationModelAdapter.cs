﻿using System;

namespace Domain.Entities.ValueObjects.Adapter
{
  
    public partial class ReservationModelAdapter
    {
        /// <summary>
        /// Responsible for convert model to entity
        /// </summary>
        /// <param name="reservationModel"></param>
        /// <returns>Returns Reservation entity </returns>
        public static Reservation Create(ReservationModel reservationModel)
        {
            if (reservationModel == null) return null;
            return new Reservation
            {
                Contact = new Contact { ContactId = reservationModel.ContactId},
                Description = reservationModel.Description,
                Date = reservationModel.Date,
                Ranking = reservationModel.Ranking,
                ReservationId = reservationModel.ReservationId,
                ReservationStatus = new ReservationStatus{ReservationStatusId = reservationModel.ReservationId}
            };
        }

        /// <summary>
        /// Responsible for convert entity to model
        /// </summary>
        /// <param name="reservation"></param>
        /// <returns>Returns ReservationModel entity </returns>
        public static ReservationModel Create(Reservation reservation)
        {
            if (reservation == null) return null;
            return new ReservationModel
            {
                ContactId = reservation.Contact?.ContactId ?? 0,
                Description = reservation.Description,
                Date = reservation.Date,
                ReservationId = reservation.ReservationId,
                Ranking = reservation.Ranking,
                StatusId = reservation.ReservationStatus?.ReservationStatusId ?? 0
            };
        }


    }
}
