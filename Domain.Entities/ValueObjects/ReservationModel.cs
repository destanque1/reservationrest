﻿using System;
using System.Runtime.Serialization;

namespace Domain.Entities.ValueObjects
{
    [DataContract]
    public partial class ReservationModel
    {

        [DataMember] public int ReservationId { get; set; }
        [DataMember] public string Description { get; set; }
        [DataMember] public int Ranking { get; set; }
        [DataMember] public DateTime Date { get; set; }
        [DataMember] public int ContactId { get; set; }
        [DataMember] public int StatusId { get; set; }




    }
}
