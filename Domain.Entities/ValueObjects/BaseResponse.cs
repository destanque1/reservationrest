﻿using System.Collections.Generic;
using CrossCutting.Exceptions;
using Domain.Entities.Abstracts;

namespace Domain.Entities.ValueObjects
{
    public class BaseResponse : ValueObject<BaseResponse>
    {
        public int Code { get; set; }

        public List<SingleError> Errors { get; set; }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object>
            {
                Code,
                Errors
            };
        }
    }
}
