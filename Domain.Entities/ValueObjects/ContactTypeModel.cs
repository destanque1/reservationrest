﻿using System.Runtime.Serialization;

namespace Domain.Entities.ValueObjects
{
    [DataContract]
    public partial class ContactTypeModel
    {
        [DataMember] public int ContactTypeId { get; set; }
        [DataMember] public string Code { get; set; }
        [DataMember] public string Description { get; set; }

    }
}
