﻿using System;
using System.Runtime.Serialization;

namespace Domain.Entities.ValueObjects
{
    [DataContract]
    public partial class ContactModel
    {

        [DataMember] public int ContactId { get; set; }

        [DataMember] public string Name { get; set; }

        [DataMember] public string Phone { get; set; }

        [DataMember] public int ContactTypeId { get; set; }
        [DataMember] public DateTime BirthDate { get; set; }




    }
}
