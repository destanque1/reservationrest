﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Domain.Entities.Migrations
{
    public partial class MyFirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "schema");

            migrationBuilder.CreateTable(
                name: "ContactType",
                schema: "schema",
                columns: table => new
                {
                    ContactTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactType", x => x.ContactTypeId);
                });

            migrationBuilder.CreateTable(
                name: "ReservationStatus",
                schema: "schema",
                columns: table => new
                {
                    ReservationStatusId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReservationStatus", x => x.ReservationStatusId);
                });

            migrationBuilder.CreateTable(
                name: "Contact",
                schema: "schema",
                columns: table => new
                {
                    ContactId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    BirthDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TypeId = table.Column<int>(type: "int", nullable: false),
                    Rating = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contact", x => x.ContactId);
                    table.ForeignKey(
                        name: "FK_Contact_ContactType_TypeId",
                        column: x => x.TypeId,
                        principalSchema: "schema",
                        principalTable: "ContactType",
                        principalColumn: "ContactTypeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Reservation",
                schema: "schema",
                columns: table => new
                {
                    ReservationId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Ranking = table.Column<int>(type: "int", nullable: false),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ContactId = table.Column<int>(type: "int", nullable: false),
                    StatusId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reservation", x => x.ReservationId);
                    table.ForeignKey(
                        name: "FK_Reservation_Contact_ContactId",
                        column: x => x.ContactId,
                        principalSchema: "schema",
                        principalTable: "Contact",
                        principalColumn: "ContactId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Reservation_ReservationStatus_StatusId",
                        column: x => x.StatusId,
                        principalSchema: "schema",
                        principalTable: "ReservationStatus",
                        principalColumn: "ReservationStatusId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "schema",
                table: "ContactType",
                columns: new[] { "ContactTypeId", "Code", "Description" },
                values: new object[,]
                {
                    { 1, "Type1", "Type1" },
                    { 2, "Type2", "Type2" },
                    { 3, "Type3", "Type3" },
                    { 4, "Type4", "Type4" }
                });

            migrationBuilder.InsertData(
                schema: "schema",
                table: "ReservationStatus",
                columns: new[] { "ReservationStatusId", "Code", "Description" },
                values: new object[,]
                {
                    { 1, "PTC", "Pending to create" },
                    { 2, "PTU", "Pending to update" },
                    { 3, "RDY", "Ready" },
                    { 4, "CLO", "Close" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Contact_TypeId",
                schema: "schema",
                table: "Contact",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Reservation_ContactId",
                schema: "schema",
                table: "Reservation",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Reservation_StatusId",
                schema: "schema",
                table: "Reservation",
                column: "StatusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Reservation",
                schema: "schema");

            migrationBuilder.DropTable(
                name: "Contact",
                schema: "schema");

            migrationBuilder.DropTable(
                name: "ReservationStatus",
                schema: "schema");

            migrationBuilder.DropTable(
                name: "ContactType",
                schema: "schema");
        }
    }
}
