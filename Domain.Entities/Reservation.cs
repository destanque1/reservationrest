﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public partial class Reservation
    {
        public Reservation()
        {
          
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ReservationId { get; set; }

        [Required]
        public string Description { get; set; }
        
        public int Ranking { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        [ForeignKey("ContactId")]
        public Contact Contact { get; set; }

        [Required]
        [ForeignKey("StatusId")]
        public ReservationStatus ReservationStatus { get; set; }







    }
}
