﻿namespace Domain.Entities.Conts
{
    public static class LocationConsts
    {
        public const string Reservation = "Reservation";
        public const string Contact = "Contact";
        public const string ContactType = "ContactType";
        public const string UpdateReservationStatus = "UpdateReservationStatus";
    }
}
