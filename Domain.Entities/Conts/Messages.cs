﻿namespace Domain.Entities.Conts
{
    public static class Messages
    {
        public const string ReservationNotFound = "Reservation not found";
        public const string ContactNotFound = "Contact not found";
        public const string ContactNameAlreadyExist = "Contact name already exist";
        public const string ContactTypeNotFound = "ContactType not found";
        public const string ReservationStatusNotFound = "Reservation status not found";
        public const string NotNullModel = "The model is required for this request";
        public const string ReservationContactAlreadyExist = "Reservation for this contact already exist in the date";
        public const string ReservationContactNotChange = "The reservation contact cannot be changed";
        public const string ContactInReservation = "The contact has reservations actives";
    }
}
