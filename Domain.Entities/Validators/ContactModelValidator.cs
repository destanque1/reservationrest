﻿using System;
using Domain.Entities.ValueObjects;
using FluentValidation;

namespace Domain.Entities.Validators
{
    public class ContactModelValidator : AbstractValidator<ContactModel>
    {
        public ContactModelValidator()
        {
            RuleFor(contact => contact.Name).NotNull().Length(1, 250);
            RuleFor(contact => contact.BirthDate).NotNull().LessThan(DateTime.Today);
            RuleFor(contact => contact.ContactTypeId).NotNull().GreaterThan(0);
            RuleFor(contact => contact.Phone).Matches("^[0-9]*$").Length(8, 16);
        }
    }
}
