﻿using System;
using Domain.Entities.ValueObjects;
using FluentValidation;

namespace Domain.Entities.Validators
{
    public class ReservationModelValidator : AbstractValidator<ReservationModel>
    {
        public ReservationModelValidator()
        {
            RuleFor(reservation => reservation.Date).NotNull().GreaterThanOrEqualTo(DateTime.Today);
            RuleFor(reservation => reservation.ContactId).NotNull().GreaterThan(0);
            RuleFor(reservation => reservation.Description).NotNull().MinimumLength(10);
        }
    }
}
