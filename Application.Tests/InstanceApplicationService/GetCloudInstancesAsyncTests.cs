﻿using System;
using System.Diagnostics.CodeAnalysis;
using Application.Dtos;
using Application.IServices;
using CrossCutting.Dtos;
using DataAccess.Repositories;
using Domain.Entities;
using Domain.Entities.Contexts;
using Domain.IRepositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace Application.Tests.InstanceApplicationService
{
    [ExcludeFromCodeCoverage]
    public class GetCloudInstancesAsyncTests : IDisposable
    {
        private readonly CasContext _dbContext;
        private IUnitOfWork _unitOfWork;
        private IInstanceApplicationService _instanceApplicationService;

        public GetCloudInstancesAsyncTests()
        {
            var options = new DbContextOptionsBuilder<CasContext>()
                .UseInMemoryDatabase($"UpdateTests{Guid.NewGuid()}")
                .Options;

            _dbContext = new CasContext(options);
            _dbContext.Companies.Add(new Companies { CompanyId = 1, Active = true, OrganizationId = 1});
            _dbContext.Companies.Add(new Companies { CompanyId = 403, Active = true, OrganizationId = 404 });
            _dbContext.Provision.Add(new Provision { Active = true, OrganizationId = 1, ProvisionId = 1});
            _dbContext.Instance.Add(new Instance { InstanceId = 1, ProvisionId = 1, Active = true, Description = "Des", ServerType = new ServerType { Code = "App" }}) ;
            _dbContext.Instance.Add(new Instance { InstanceId = 2, ProvisionId = 1, Active = true, ServerType = new ServerType { Code = "vws" }}) ;
            _dbContext.Instance.Add(new Instance { InstanceId = 4, ProvisionId = 1, Active = true, ServerType = new ServerType { Code = "" } }) ;
            _dbContext.Instance.Add(new Instance { InstanceId = 5, ProvisionId = 1, Active = true, ServerType = new ServerType { Code = null } }) ;
            _dbContext.Instance.Add(new Instance { InstanceId = 6, ProvisionId = 1, Active = true, ServerType = new ServerType { Code = "FBU" } }) ;
            _dbContext.SaveChanges();
            _unitOfWork = new UnitOfWork(_dbContext);
            _instanceApplicationService = new Services.InstanceApplicationService(_unitOfWork);

        }

        public void Dispose()
        {
            _dbContext.Dispose();
            _unitOfWork = null;
        }
        [Fact]
        public async void DeploymentIdZero()
        {
            var result = await _instanceApplicationService.GetCloudInstancesAsync(0);
            Assert.IsAssignableFrom<NotFoundObjectResult>(result.Result);
        }

        [Fact]
        public async void DeploymentNotFound()
        {
            var result = await _instanceApplicationService.GetCloudInstancesAsync(404);
            Assert.IsAssignableFrom<NotFoundObjectResult>(result.Result);
        }

        [Fact]
        public async void ProvisionNotFound()
        {
            var result = await _instanceApplicationService.GetCloudInstancesAsync(403);
            Assert.IsAssignableFrom<NotFoundObjectResult>(result.Result);
        }

        //[Fact]
        //public async void EmptyList()
        //{
        //    var result = await instanceApplicationService.GetCloudInstancesAsync(404);
        //    Assert.IsAssignableFrom<ListDto<CloudInstanceDto>>(result.Value);
        //}

        [Fact]
        public async void Paged()
        {
            
            var result = await _instanceApplicationService.GetCloudInstancesAsync(1,1,6);
            Assert.IsAssignableFrom<ListDto<CloudInstanceDto>>(result.Value);
            Assert.Equal(5, result.Value.Count);
            Assert.Equal(5, result.Value.List.Count);
        }
    }

}
