﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Application.Dtos;
using Application.Tests.Helpers;
using DataAccess.Repositories;
using Domain.Entities;
using Domain.Entities.Contexts;
using Domain.IRepositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;

namespace Application.Tests.InstanceApplicationService
{
    [ExcludeFromCodeCoverage]
    public class GetCloudInstanceCredentialTests : IDisposable
    {
        
        private CasContext _dbContext { get
            {
                var options = new DbContextOptionsBuilder<CasContext>()
                .UseInMemoryDatabase($"UpdateTests{Guid.NewGuid()}")
                .Options;
                return new CasContext(options);
            }
        }
        public void Dispose()
        {
            _dbContext.Dispose();
        }
        [Fact]
        public async void DeploymentIdZero()
        {
            var unitOfWork = new UnitOfWork(_dbContext);
            var instanceApplicationService = new Services.InstanceApplicationService(unitOfWork);
            var result = await instanceApplicationService.GetCloudInstanceCredential(0, 4);
            Assert.IsAssignableFrom<NotFoundObjectResult>(result.Result);
        }

        [Fact]
        public async void InstanceIdZero()
        {
            var unitOfWork = new UnitOfWork(_dbContext);
            var instanceApplicationService = new Services.InstanceApplicationService(unitOfWork);
            var result = await instanceApplicationService.GetCloudInstanceCredential(4, 0);
            Assert.IsAssignableFrom<NotFoundObjectResult>(result.Result);
        }

        [Fact]
        public async void DeploymentNotFound()
        {
            var unitOfWork = new UnitOfWork(_dbContext);
            var instanceApplicationService = new Services.InstanceApplicationService(unitOfWork);
            var result = await instanceApplicationService.GetCloudInstanceCredential(404, 404);
            Assert.IsAssignableFrom<NotFoundObjectResult>(result.Result);
        }

        [Fact]
        public async void ProvisionNotFound()
        {
            var mockUnicOfWork = new Mock<IUnitOfWork>();

            var companiesList = new List<Companies>{new Companies
            {
                OrganizationId = 404
            }};

            mockUnicOfWork.Setup(m => m.CompaniesRepository.GetQueryBy(It.IsAny<Expression<Func<Companies, bool>>>(),
                It.IsAny<Func<IQueryable<Companies>, IOrderedQueryable<Companies>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Companies>(companiesList.AsEnumerable()));

            mockUnicOfWork.Setup(m => m.DesktopProvisionRepository.GetQueryBy(It.IsAny<Expression<Func<DesktopProvision, bool>>>(),
                It.IsAny<Func<IQueryable<DesktopProvision>, IOrderedQueryable<DesktopProvision>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<DesktopProvision>(new List<DesktopProvision>().AsEnumerable()));

            mockUnicOfWork.Setup(m => m.InstanceRepository.GetQueryBy(It.IsAny<Expression<Func<Instance, bool>>>(),
                It.IsAny<Func<IQueryable<Instance>, IOrderedQueryable<Instance>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Instance>(new List<Instance>().AsEnumerable()));

            var instanceApplicationService = new Services.InstanceApplicationService(mockUnicOfWork.Object);
            var result = await instanceApplicationService.GetCloudInstanceCredential(404, 404);
            Assert.IsAssignableFrom<NotFoundObjectResult>(result.Result);
        }

        [Fact]
        public async void InstanceNotFound()
        {
            var mockUnicOfWork = new Mock<IUnitOfWork>();

            var companiesList = new List<Companies>{new Companies
            {
                OrganizationId = 404
            }};

            mockUnicOfWork.Setup(m => m.CompaniesRepository.GetQueryBy(It.IsAny<Expression<Func<Companies, bool>>>(),
                It.IsAny<Func<IQueryable<Companies>, IOrderedQueryable<Companies>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Companies>(companiesList.AsEnumerable()));

            mockUnicOfWork.Setup(m => m.DesktopProvisionRepository.GetOneByAsync(It.IsAny<Expression<Func<DesktopProvision, bool>>>(), It.IsAny<Func<IQueryable<DesktopProvision>, IOrderedQueryable<DesktopProvision>>>(), It.IsAny<bool>())).Returns(Task.FromResult(new DesktopProvision()));

            mockUnicOfWork.Setup(m => m.InstanceRepository.GetOneByAsync(It.IsAny<Expression<Func<Instance, bool>>>(), It.IsAny<Func<IQueryable<Instance>, IOrderedQueryable<Instance>>>(), It.IsAny<bool>()))
                .Returns(Task.FromResult<Instance>(null));

            var instanceApplicationService = new Services.InstanceApplicationService(mockUnicOfWork.Object);
            var result = await instanceApplicationService.GetCloudInstanceCredential(404, 404);
            Assert.IsAssignableFrom<NotFoundObjectResult>(result.Result);
        }

        [Fact]
        public async void ExistInstanceCredentials()
        {
            var mockUnicOfWork = new Mock<IUnitOfWork>();

            var companiesList = new List<Companies>{new Companies
            {
                OrganizationId = 404
            }};

            mockUnicOfWork.Setup(m => m.CompaniesRepository.GetQueryBy(It.IsAny<Expression<Func<Companies, bool>>>(),
                It.IsAny<Func<IQueryable<Companies>, IOrderedQueryable<Companies>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Companies>(companiesList.AsEnumerable()));

            mockUnicOfWork.Setup(m => m.DesktopProvisionRepository.GetOneByAsync(It.IsAny<Expression<Func<DesktopProvision, bool>>>(), It.IsAny<Func<IQueryable<DesktopProvision>, IOrderedQueryable<DesktopProvision>>>(), It.IsAny<bool>())).Returns(Task.FromResult(new DesktopProvision()));

            mockUnicOfWork.Setup(m => m.InstanceRepository.AnyAsync(It.IsAny<Expression<Func<Instance, bool>>>())).Returns(Task.FromResult(true));

            mockUnicOfWork.Setup(m => m.InstanceCredentialRepository.GetOneByAsync(It.IsAny<Expression<Func<InstanceCredential, bool>>>(), It.IsAny<Func<IQueryable<InstanceCredential>, IOrderedQueryable<InstanceCredential>>>(), It.IsAny<bool>())).Returns(Task.FromResult(new InstanceCredential()));

            var instanceApplicationService = new Services.InstanceApplicationService(mockUnicOfWork.Object);
            var result = await instanceApplicationService.GetCloudInstanceCredential(404, 404);
            Assert.IsAssignableFrom<CloudInstanceCredentialDto>(result.Value);
        }

        [Fact]
        public async void NoExistInstanceCredentials()
        {
            var mockUnicOfWork = new Mock<IUnitOfWork>();

            var companiesList = new List<Companies>{new Companies
            {
                OrganizationId = 404
            }};

            mockUnicOfWork.Setup(m => m.CompaniesRepository.GetQueryBy(It.IsAny<Expression<Func<Companies, bool>>>(),
                It.IsAny<Func<IQueryable<Companies>, IOrderedQueryable<Companies>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Companies>(companiesList.AsEnumerable()));

            mockUnicOfWork.Setup(m => m.DesktopProvisionRepository.GetOneByAsync(It.IsAny<Expression<Func<DesktopProvision, bool>>>(), It.IsAny<Func<IQueryable<DesktopProvision>, IOrderedQueryable<DesktopProvision>>>(), It.IsAny<bool>())).Returns(Task.FromResult(new DesktopProvision()));

            mockUnicOfWork.Setup(m => m.InstanceRepository.AnyAsync(It.IsAny<Expression<Func<Instance, bool>>>())).Returns(Task.FromResult(true));

            mockUnicOfWork.Setup(m => m.InstanceCredentialRepository.GetOneByAsync(It.IsAny<Expression<Func<InstanceCredential, bool>>>(), It.IsAny<Func<IQueryable<InstanceCredential>, IOrderedQueryable<InstanceCredential>>>(), It.IsAny<bool>())).Returns(Task.FromResult<InstanceCredential>(null));

            var instanceApplicationService = new Services.InstanceApplicationService(mockUnicOfWork.Object);
            var result = await instanceApplicationService.GetCloudInstanceCredential(404, 404);
            Assert.IsAssignableFrom<CloudInstanceCredentialDto>(result.Value);
        }
    }
}
