﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Application.Dtos;
using Application.Tests.Helpers;
using Domain.Entities;
using Domain.IRepositories;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Application.Tests.InstanceApplicationService
{
    [ExcludeFromCodeCoverage]
    public class GetCloudInstanceByAsyncTests
    {
        [Fact]
        public async void DeploymentIdZero()
        {
            var mockUnicOfWork = new Mock<IUnitOfWork>();
            var instanceApplicationService = new Services.InstanceApplicationService(mockUnicOfWork.Object);
            var result = await instanceApplicationService.GetCloudInstanceByAsync(0,4);
            Assert.IsAssignableFrom<NotFoundObjectResult>(result.Result);
        }

        [Fact]
        public async void InstanceIdZero()
        {
            var mockUnicOfWork = new Mock<IUnitOfWork>();
            var instanceApplicationService = new Services.InstanceApplicationService(mockUnicOfWork.Object);
            var result = await instanceApplicationService.GetCloudInstanceByAsync(4, 0);
            Assert.IsAssignableFrom<NotFoundObjectResult>(result.Result);
        }

        [Fact]
        public async void DeploymentNotFound()
        {
            var mockUnicOfWork = new Mock<IUnitOfWork>();

            mockUnicOfWork.Setup(m => m.CompaniesRepository.GetQueryBy(It.IsAny<Expression<Func<Companies, bool>>>(),
                It.IsAny<Func<IQueryable<Companies>, IOrderedQueryable<Companies>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Companies>(new List<Companies>()));

            var instanceApplicationService = new Services.InstanceApplicationService(mockUnicOfWork.Object);
            var result = await instanceApplicationService.GetCloudInstanceByAsync(404, 404);
            Assert.IsAssignableFrom<NotFoundObjectResult>(result.Result);
        }

        [Fact]
        public async void ProvisionNotFound()
        {
            var mockUnicOfWork = new Mock<IUnitOfWork>();

            var companiesList = new List<Companies>{new Companies
            {
                OrganizationId = 404
            }};

            mockUnicOfWork.Setup(m => m.CompaniesRepository.GetQueryBy(It.IsAny<Expression<Func<Companies, bool>>>(),
                It.IsAny<Func<IQueryable<Companies>, IOrderedQueryable<Companies>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Companies>(companiesList.AsEnumerable()));

            mockUnicOfWork.Setup(m => m.ProvisionRepository.GetQueryBy(It.IsAny<Expression<Func<Provision, bool>>>(),
                It.IsAny<Func<IQueryable<Provision>, IOrderedQueryable<Provision>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Provision>(new List<Provision>().AsEnumerable()));

            mockUnicOfWork.Setup(m => m.InstanceRepository.GetQueryBy(It.IsAny<Expression<Func<Instance, bool>>>(),
                It.IsAny<Func<IQueryable<Instance>, IOrderedQueryable<Instance>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Instance>(new List<Instance>().AsEnumerable()));

            var instanceApplicationService = new Services.InstanceApplicationService(mockUnicOfWork.Object);
            var result = await instanceApplicationService.GetCloudInstanceByAsync(404, 404);
            Assert.IsAssignableFrom<NotFoundObjectResult>(result.Result);
        }

        [Fact]
        public async void InstanceNotFound()
        {
            var mockUnicOfWork = new Mock<IUnitOfWork>();

            var companiesList = new List<Companies>{new Companies
            {
                OrganizationId = 404
            }};

            var provisionList = new List<Provision>{new Provision
            {
                ProvisionId = 1
            }};

            mockUnicOfWork.Setup(m => m.CompaniesRepository.GetQueryBy(It.IsAny<Expression<Func<Companies, bool>>>(),
                It.IsAny<Func<IQueryable<Companies>, IOrderedQueryable<Companies>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Companies>(companiesList.AsEnumerable()));

            mockUnicOfWork.Setup(m => m.ProvisionRepository.GetQueryBy(It.IsAny<Expression<Func<Provision, bool>>>(),
                It.IsAny<Func<IQueryable<Provision>, IOrderedQueryable<Provision>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Provision>(provisionList.AsEnumerable()));

            mockUnicOfWork.Setup(m => m.InstanceRepository.GetOneByAsync(It.IsAny<Expression<Func<Instance, bool>>>(), It.IsAny<Func<IQueryable<Instance>, IOrderedQueryable<Instance>>>(), It.IsAny<bool>()))
                .Returns(Task.FromResult<Instance>(null));

            var instanceApplicationService = new Services.InstanceApplicationService(mockUnicOfWork.Object);
            var result = await instanceApplicationService.GetCloudInstanceByAsync(404, 404);
            Assert.IsAssignableFrom<NotFoundObjectResult>(result.Result);
        }

        [Fact]
        public async void InstanceFound()
        {
            var mockUnicOfWork = new Mock<IUnitOfWork>();

            var companiesList = new List<Companies>{new Companies
            {
                OrganizationId = 404
            }};

            var provisionList = new List<Provision>{new Provision
            {
                ProvisionId = 1
            }};

            mockUnicOfWork.Setup(m => m.CompaniesRepository.GetQueryBy(It.IsAny<Expression<Func<Companies, bool>>>(),
                It.IsAny<Func<IQueryable<Companies>, IOrderedQueryable<Companies>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Companies>(companiesList.AsEnumerable()));
            mockUnicOfWork.Setup(m => m.ProvisionRepository.GetQueryBy(It.IsAny<Expression<Func<Provision, bool>>>(),
                It.IsAny<Func<IQueryable<Provision>, IOrderedQueryable<Provision>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Provision>(provisionList.AsEnumerable()));
            mockUnicOfWork.Setup(m => m.InstanceRepository.GetOneByAsync(It.IsAny<Expression<Func<Instance, bool>>>(), It.IsAny<Func<IQueryable<Instance>, IOrderedQueryable<Instance>>>(), It.IsAny<bool>(), It.IsAny<Expression<Func<Instance, object>>>()))
                .Returns(Task.FromResult(new Instance()));
            mockUnicOfWork.Setup(m => m.ExternalIpRepository.GetAllByAsync(It.IsAny<Expression<Func<ExternalIp, bool>>>(), null, -1, -1, false)).Returns(Task.FromResult(new List<ExternalIp>
            {
                new ExternalIp
                {
                    IpAddress = "10.0.0.0"
                }
            }));
            mockUnicOfWork.Setup(m => m.InternalIpRepository.GetAllByAsync(It.IsAny<Expression<Func<InternalIp, bool>>>(), null, -1, -1, false)).Returns(Task.FromResult(new List<InternalIp>
            {
                new InternalIp
                {
                    IpAddress = "192.168.0.0"
                }
            }));
            mockUnicOfWork.Setup(m => m.DiskRepository.GetAllByAsync(It.IsAny<Expression<Func<Disk, bool>>>(), It.IsAny<Func<IQueryable<Disk>, IOrderedQueryable<Disk>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<Expression<Func<Disk, object>>>())).Returns(Task.FromResult(new List<Disk>
            {
                new Disk
                {
                    AutoDelete = true,
                    Boot = true,
                    DataDisk = false,
                    DeviceName = "Disk name",
                    Size = 50,
                    DiskType = new DiskType
                    {
                        Name = "ssd",
                        Description = "ssd"
                    }
                }
            }));
            mockUnicOfWork.Setup(m => m.PzoneRepository.GetOneByAsync(It.IsAny<Expression<Func<Pzone, bool>>>(), It.IsAny<Func<IQueryable<Pzone>, IOrderedQueryable<Pzone>>>(), It.IsAny<bool>(), It.IsAny<Expression<Func<Pzone, object>>>()))
                .Returns(Task.FromResult<Pzone>(null));

            var instanceApplicationService = new Services.InstanceApplicationService(mockUnicOfWork.Object);
            var result = await instanceApplicationService.GetCloudInstanceByAsync(404, 404);
            Assert.IsAssignableFrom<CloudInstanceDetailsDto>(result.Value);
        }

        [Fact]
        public async void InstanceFoundObjectsNull()
        {
            var mockUnicOfWork = new Mock<IUnitOfWork>();

            var companiesList = new List<Companies>{new Companies
            {
                OrganizationId = 404
            }};

            var provisionList = new List<Provision>{new Provision
            {
                ProvisionId = 1
            }};

            mockUnicOfWork.Setup(m => m.CompaniesRepository.GetQueryBy(It.IsAny<Expression<Func<Companies, bool>>>(),
                It.IsAny<Func<IQueryable<Companies>, IOrderedQueryable<Companies>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Companies>(companiesList.AsEnumerable()));
            mockUnicOfWork.Setup(m => m.ProvisionRepository.GetQueryBy(It.IsAny<Expression<Func<Provision, bool>>>(),
                It.IsAny<Func<IQueryable<Provision>, IOrderedQueryable<Provision>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Provision>(provisionList.AsEnumerable()));
            mockUnicOfWork.Setup(m => m.InstanceRepository.GetOneByAsync(It.IsAny<Expression<Func<Instance, bool>>>(), It.IsAny<Func<IQueryable<Instance>, IOrderedQueryable<Instance>>>(), It.IsAny<bool>(), It.IsAny<Expression<Func<Instance, object>>>()))
                .Returns(Task.FromResult(new Instance()));
            mockUnicOfWork.Setup(m => m.ExternalIpRepository.GetAllByAsync(It.IsAny<Expression<Func<ExternalIp, bool>>>(), null, -1, -1, false)).Returns(Task.FromResult(new List<ExternalIp>()));
            mockUnicOfWork.Setup(m => m.InternalIpRepository.GetAllByAsync(It.IsAny<Expression<Func<InternalIp, bool>>>(), null, -1, -1, false)).Returns(Task.FromResult(new List<InternalIp>()));
            mockUnicOfWork.Setup(m => m.DiskRepository.GetAllByAsync(It.IsAny<Expression<Func<Disk, bool>>>(), It.IsAny<Func<IQueryable<Disk>, IOrderedQueryable<Disk>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<Expression<Func<Disk, object>>>())).Returns(Task.FromResult(new List<Disk>
            {
                new Disk
                {
                    AutoDelete = true,
                    Boot = true,
                    DataDisk = false,
                    DeviceName = "Disk name",
                    Size = 50,
                    DiskType = null
                }
            }));
            mockUnicOfWork.Setup(m => m.PzoneRepository.GetOneByAsync(It.IsAny<Expression<Func<Pzone, bool>>>(), It.IsAny<Func<IQueryable<Pzone>, IOrderedQueryable<Pzone>>>(), It.IsAny<bool>(), It.IsAny<Expression<Func<Pzone, object>>>()))
                .Returns(Task.FromResult(new Pzone()));

            var instanceApplicationService = new Services.InstanceApplicationService(mockUnicOfWork.Object);
            var result = await instanceApplicationService.GetCloudInstanceByAsync(404, 404);
            Assert.IsAssignableFrom<CloudInstanceDetailsDto>(result.Value);
        }

        [Fact]
        public async void InstanceFoundListDiskNull()
        {
            var mockUnicOfWork = new Mock<IUnitOfWork>();

            var companiesList = new List<Companies>{new Companies
            {
                OrganizationId = 404
            }};

            var provisionList = new List<Provision>{new Provision
            {
                ProvisionId = 1
            }};

            mockUnicOfWork.Setup(m => m.CompaniesRepository.GetQueryBy(It.IsAny<Expression<Func<Companies, bool>>>(),
                It.IsAny<Func<IQueryable<Companies>, IOrderedQueryable<Companies>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Companies>(companiesList.AsEnumerable()));
            mockUnicOfWork.Setup(m => m.ProvisionRepository.GetQueryBy(It.IsAny<Expression<Func<Provision, bool>>>(),
                It.IsAny<Func<IQueryable<Provision>, IOrderedQueryable<Provision>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Provision>(provisionList.AsEnumerable()));
            mockUnicOfWork.Setup(m => m.InstanceRepository.GetOneByAsync(It.IsAny<Expression<Func<Instance, bool>>>(), It.IsAny<Func<IQueryable<Instance>, IOrderedQueryable<Instance>>>(), It.IsAny<bool>(), It.IsAny<Expression<Func<Instance, object>>>()))
                .Returns(Task.FromResult(new Instance { ZoneId = 1}));
            mockUnicOfWork.Setup(m => m.ExternalIpRepository.GetAllByAsync(It.IsAny<Expression<Func<ExternalIp, bool>>>(), null, -1, -1, false)).Returns(Task.FromResult(new List<ExternalIp>()));
            mockUnicOfWork.Setup(m => m.InternalIpRepository.GetAllByAsync(It.IsAny<Expression<Func<InternalIp, bool>>>(), null, -1, -1, false)).Returns(Task.FromResult(new List<InternalIp>()));
            mockUnicOfWork.Setup(m => m.DiskRepository.GetAllByAsync(It.IsAny<Expression<Func<Disk, bool>>>(), It.IsAny<Func<IQueryable<Disk>, IOrderedQueryable<Disk>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<Expression<Func<Disk, object>>>())).Returns(Task.FromResult<List<Disk>>(null));
            mockUnicOfWork.Setup(m => m.PzoneRepository.GetOneByAsync(It.IsAny<Expression<Func<Pzone, bool>>>(), It.IsAny<Func<IQueryable<Pzone>, IOrderedQueryable<Pzone>>>(), It.IsAny<bool>(), It.IsAny<Expression<Func<Pzone, object>>>()))
                .Returns(Task.FromResult(new Pzone()));

            var instanceApplicationService = new Services.InstanceApplicationService(mockUnicOfWork.Object);
            var result = await instanceApplicationService.GetCloudInstanceByAsync(404, 404);
            Assert.IsAssignableFrom<CloudInstanceDetailsDto>(result.Value);
        }

        [Fact]
        public async void InstanceFoundApp()
        {
            var mockUnicOfWork = new Mock<IUnitOfWork>();

            var companiesList = new List<Companies>{new Companies
            {
                OrganizationId = 404
            }};

            var provisionList = new List<Provision>{new Provision
            {
                ProvisionId = 1
            }};

            mockUnicOfWork.Setup(m => m.CompaniesRepository.GetQueryBy(It.IsAny<Expression<Func<Companies, bool>>>(),
                It.IsAny<Func<IQueryable<Companies>, IOrderedQueryable<Companies>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Companies>(companiesList.AsEnumerable()));
            mockUnicOfWork.Setup(m => m.ProvisionRepository.GetQueryBy(It.IsAny<Expression<Func<Provision, bool>>>(),
                It.IsAny<Func<IQueryable<Provision>, IOrderedQueryable<Provision>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Provision>(provisionList.AsEnumerable()));
            mockUnicOfWork.Setup(m => m.InstanceRepository.GetOneByAsync(It.IsAny<Expression<Func<Instance, bool>>>(), It.IsAny<Func<IQueryable<Instance>, IOrderedQueryable<Instance>>>(), It.IsAny<bool>(), It.IsAny<Expression<Func<Instance, object>>>()))
                .Returns(Task.FromResult(new Instance { InstanceId = 1, Description = "Des", ServerType = new ServerType { Code = "App" } }));
            mockUnicOfWork.Setup(m => m.ExternalIpRepository.GetAllByAsync(It.IsAny<Expression<Func<ExternalIp, bool>>>(), null, -1, -1, false)).Returns(Task.FromResult(new List<ExternalIp>()));
            mockUnicOfWork.Setup(m => m.InternalIpRepository.GetAllByAsync(It.IsAny<Expression<Func<InternalIp, bool>>>(), null, -1, -1, false)).Returns(Task.FromResult(new List<InternalIp>()));
            mockUnicOfWork.Setup(m => m.DiskRepository.GetAllByAsync(It.IsAny<Expression<Func<Disk, bool>>>(), It.IsAny<Func<IQueryable<Disk>, IOrderedQueryable<Disk>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<Expression<Func<Disk, object>>>())).Returns(Task.FromResult<List<Disk>>(null));
            mockUnicOfWork.Setup(m => m.PzoneRepository.GetOneByAsync(It.IsAny<Expression<Func<Pzone, bool>>>(), It.IsAny<Func<IQueryable<Pzone>, IOrderedQueryable<Pzone>>>(), It.IsAny<bool>(), It.IsAny<Expression<Func<Pzone, object>>>()))
                .Returns(Task.FromResult(new Pzone()));

            var instanceApplicationService = new Services.InstanceApplicationService(mockUnicOfWork.Object);
            var result = await instanceApplicationService.GetCloudInstanceByAsync(404, 404);
            Assert.IsAssignableFrom<CloudInstanceDetailsDto>(result.Value);
        }

        [Fact]
        public async void InstanceFoundVws()
        {
            var mockUnicOfWork = new Mock<IUnitOfWork>();

            var companiesList = new List<Companies>{new Companies
            {
                OrganizationId = 404
            }};

            var provisionList = new List<Provision>{new Provision
            {
                ProvisionId = 1
            }};

            mockUnicOfWork.Setup(m => m.CompaniesRepository.GetQueryBy(It.IsAny<Expression<Func<Companies, bool>>>(),
                It.IsAny<Func<IQueryable<Companies>, IOrderedQueryable<Companies>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Companies>(companiesList.AsEnumerable()));
            mockUnicOfWork.Setup(m => m.ProvisionRepository.GetQueryBy(It.IsAny<Expression<Func<Provision, bool>>>(),
                It.IsAny<Func<IQueryable<Provision>, IOrderedQueryable<Provision>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Provision>(provisionList.AsEnumerable()));
            mockUnicOfWork.Setup(m => m.InstanceRepository.GetOneByAsync(It.IsAny<Expression<Func<Instance, bool>>>(), It.IsAny<Func<IQueryable<Instance>, IOrderedQueryable<Instance>>>(), It.IsAny<bool>(), It.IsAny<Expression<Func<Instance, object>>>()))
                .Returns(Task.FromResult(new Instance { InstanceId = 2, ServerType = new ServerType { Code = "vws" } }));
            mockUnicOfWork.Setup(m => m.ExternalIpRepository.GetAllByAsync(It.IsAny<Expression<Func<ExternalIp, bool>>>(), null, -1, -1, false)).Returns(Task.FromResult(new List<ExternalIp>()));
            mockUnicOfWork.Setup(m => m.InternalIpRepository.GetAllByAsync(It.IsAny<Expression<Func<InternalIp, bool>>>(), null, -1, -1, false)).Returns(Task.FromResult(new List<InternalIp>()));
            mockUnicOfWork.Setup(m => m.DiskRepository.GetAllByAsync(It.IsAny<Expression<Func<Disk, bool>>>(), It.IsAny<Func<IQueryable<Disk>, IOrderedQueryable<Disk>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<Expression<Func<Disk, object>>>())).Returns(Task.FromResult<List<Disk>>(null));
            mockUnicOfWork.Setup(m => m.PzoneRepository.GetOneByAsync(It.IsAny<Expression<Func<Pzone, bool>>>(), It.IsAny<Func<IQueryable<Pzone>, IOrderedQueryable<Pzone>>>(), It.IsAny<bool>(), It.IsAny<Expression<Func<Pzone, object>>>()))
                .Returns(Task.FromResult(new Pzone()));

            var instanceApplicationService = new Services.InstanceApplicationService(mockUnicOfWork.Object);
            var result = await instanceApplicationService.GetCloudInstanceByAsync(404, 404);
            Assert.IsAssignableFrom<CloudInstanceDetailsDto>(result.Value);
        }

        [Fact]
        public async void InstanceFoundFbu()
        {
            var mockUnicOfWork = new Mock<IUnitOfWork>();

            var companiesList = new List<Companies>{new Companies
            {
                OrganizationId = 404
            }};

            var provisionList = new List<Provision>{new Provision
            {
                ProvisionId = 1
            }};

            mockUnicOfWork.Setup(m => m.CompaniesRepository.GetQueryBy(It.IsAny<Expression<Func<Companies, bool>>>(),
                It.IsAny<Func<IQueryable<Companies>, IOrderedQueryable<Companies>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Companies>(companiesList.AsEnumerable()));
            mockUnicOfWork.Setup(m => m.ProvisionRepository.GetQueryBy(It.IsAny<Expression<Func<Provision, bool>>>(),
                It.IsAny<Func<IQueryable<Provision>, IOrderedQueryable<Provision>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new TestAsyncEnumerable<Provision>(provisionList.AsEnumerable()));
            mockUnicOfWork.Setup(m => m.InstanceRepository.GetOneByAsync(It.IsAny<Expression<Func<Instance, bool>>>(), It.IsAny<Func<IQueryable<Instance>, IOrderedQueryable<Instance>>>(), It.IsAny<bool>(), It.IsAny<Expression<Func<Instance, object>>>()))
                .Returns(Task.FromResult(new Instance { InstanceId = 5, ServerType = new ServerType { Code = "FBU" } }));
            mockUnicOfWork.Setup(m => m.ExternalIpRepository.GetAllByAsync(It.IsAny<Expression<Func<ExternalIp, bool>>>(), null, -1, -1, false)).Returns(Task.FromResult(new List<ExternalIp>()));
            mockUnicOfWork.Setup(m => m.InternalIpRepository.GetAllByAsync(It.IsAny<Expression<Func<InternalIp, bool>>>(), null, -1, -1, false)).Returns(Task.FromResult(new List<InternalIp>()));
            mockUnicOfWork.Setup(m => m.DiskRepository.GetAllByAsync(It.IsAny<Expression<Func<Disk, bool>>>(), It.IsAny<Func<IQueryable<Disk>, IOrderedQueryable<Disk>>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<Expression<Func<Disk, object>>>())).Returns(Task.FromResult<List<Disk>>(null));
            mockUnicOfWork.Setup(m => m.PzoneRepository.GetOneByAsync(It.IsAny<Expression<Func<Pzone, bool>>>(), It.IsAny<Func<IQueryable<Pzone>, IOrderedQueryable<Pzone>>>(), It.IsAny<bool>(), It.IsAny<Expression<Func<Pzone, object>>>()))
                .Returns(Task.FromResult(new Pzone()));

            var instanceApplicationService = new Services.InstanceApplicationService(mockUnicOfWork.Object);
            var result = await instanceApplicationService.GetCloudInstanceByAsync(404, 404);
            Assert.IsAssignableFrom<CloudInstanceDetailsDto>(result.Value);
        }
    }
}
