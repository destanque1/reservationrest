﻿using Application.Dtos;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Application.Tests.Application.Dtos
{
    [ExcludeFromCodeCoverage]
    public class CloudInstanceDtoTests
    {
        [Fact]
        public void TestGets()
        {
            var cloudInstance = new CloudInstanceDto
            {
                Active = true,
                Code = "Code",
                Description = "Description",
                Domain = true,
                InstanceId = 1,
                InstanceTypeId = 1,
                Name = "Name"
            };

            Assert.True(cloudInstance.Active);
            Assert.Equal("Code", cloudInstance.Code);
            Assert.Equal("Description", cloudInstance.Description);
            Assert.True(cloudInstance.Domain);
            Assert.Equal(1, cloudInstance.InstanceId);
            Assert.Equal(1, cloudInstance.InstanceTypeId);
            Assert.Equal("Name", cloudInstance.Name);
        }
    }
}
