﻿using Application.Dtos;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Application.Tests.Application.Dtos
{
    [ExcludeFromCodeCoverage]
    public class DiskDetailsDtoTests
    {
        [Fact]
        public void GetTests()
        {
            var diskDetail = new DiskDetailsDto
            {
                DiskId = 1,
                AutoDelete = true,
                Boot = true,
                DataDisk = true,
                DiskName = "DiskName",
                DiskSize = 10,
                DiskType = "DiskType",
                DiskTypeDescription = "DiskTypeDescription"
            };

            Assert.Equal(1, diskDetail.DiskId);
            Assert.True(diskDetail.AutoDelete);
            Assert.True(diskDetail.Boot);
            Assert.True(diskDetail.DataDisk);
            Assert.Equal("DiskName", diskDetail.DiskName);
            Assert.Equal(10, diskDetail.DiskSize);
            Assert.Equal("DiskType", diskDetail.DiskType);
            Assert.Equal("DiskTypeDescription", diskDetail.DiskTypeDescription);
        }
    }
}
