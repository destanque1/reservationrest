﻿using Application.Dtos;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Application.Tests.Application.Dtos
{
    [ExcludeFromCodeCoverage]
    public class CloudInstanceCredentialDtoTests
    {
        [Fact]
        public void TestGets()
        {
            var instanceCredential = new CloudInstanceCredentialDto
            {
                Domain = "Domain",
                Id = 1,
                Password = "Password",
                PublicKey = "PublicKey",
                Username = "Username"
            };
            Assert.Equal("Domain", instanceCredential.Domain);
            Assert.Equal(1, instanceCredential.Id);
            Assert.Equal("Password", instanceCredential.Password);
            Assert.Equal("PublicKey", instanceCredential.PublicKey);
            Assert.Equal("Username", instanceCredential.Username);
        }
    }
}
