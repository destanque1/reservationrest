﻿using Application.Dtos;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace Application.Tests.Application.Dtos
{
    [ExcludeFromCodeCoverage]
    public class CloudInstanceDetailsDtoTests
    {
        [Fact]
        public void TestGets()
        {
            var instanceDetails = new CloudInstanceDetailsDto
            {
                Zone = "Zone",
                InternalIp = new List<string>{ "192.168.0.1","145.12.23.56"},
                PublicIp = new List<string> { "10.2.3.2", "20.32.45.2" },
                Disks = new List<DiskDetailsDto>()
            };

            Assert.Equal("Zone", instanceDetails.Zone);
            Assert.Equal(2, instanceDetails.InternalIp.Count());
            Assert.Equal(2, instanceDetails.PublicIp.Count());
            Assert.Empty(instanceDetails.Disks);
        }
    }
}
