﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Domain.Entities;
using Xunit;

namespace Domain.Tests.Domain.Entities.Entities
{
    [ExcludeFromCodeCoverage]
    public class ServerTypeTests
    {
        private static readonly ServerType ServerType = new ServerType
        {
            Instance = new List<Instance>(),
            Code = "Code",
            ServerTypeId = 1,
            DefaultDiskSize = 1,
            DefaultDiskType = 1,
            DefaultMachineType = 1,
            Type = "Type"
        };

        [Fact]
        public void GetServerTypeData()
        {
            Assert.NotNull(ServerType.Instance);
            Assert.Equal("Code", ServerType.Code);
            Assert.Equal(1,ServerType.ServerTypeId);
            Assert.Equal(1,ServerType.DefaultDiskSize);
            Assert.Equal(1,ServerType.DefaultDiskType);
            Assert.Equal(1,ServerType.DefaultMachineType);
            Assert.Equal("Type",ServerType.Type);
        }
    }
}
