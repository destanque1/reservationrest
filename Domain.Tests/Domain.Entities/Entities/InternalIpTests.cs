﻿using System.Diagnostics.CodeAnalysis;
using Domain.Entities;
using Xunit;

namespace Domain.Tests.Domain.Entities.Entities
{
    [ExcludeFromCodeCoverage]
    public class InternalIpTests
    {
        private static readonly InternalIp InternalIp = new InternalIp
        {
            Instance = new Instance(),
            InstanceId = 1,
            IpAddress = "IpAddress",
            InternalIpId = 1
        };

        [Fact]
        public void GetInternalIpData()
        {
            Assert.NotNull(InternalIp.Instance);
            Assert.Equal(1, InternalIp.InstanceId);
            Assert.Equal("IpAddress", InternalIp.IpAddress);
            Assert.Equal(1, InternalIp.InternalIpId);
        }

    }
}
