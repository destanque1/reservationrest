﻿using System.Diagnostics.CodeAnalysis;
using Domain.Entities;
using Xunit;

namespace Domain.Tests.Domain.Entities.Entities
{
    [ExcludeFromCodeCoverage]
    public class InstanceInstanceCredentialTests
    {
        private static readonly InstanceInstanceCredential Credential = new InstanceInstanceCredential
        {
            Instance = new Instance(),
            Active = true,
            InstanceId = 1,
            InstanceCredential = new InstanceCredential(),
            InstanceCredentialId = 1
        };

        [Fact]
        public void GetInstanceInstanceCredentialData()
        {
            Assert.NotNull(Credential.Instance);
            Assert.True(Credential.Active);
            Assert.Equal(1, Credential.InstanceId);
            Assert.NotNull(Credential.InstanceCredential);
            Assert.Equal(1, Credential.InstanceCredentialId);
        }
    }
}
