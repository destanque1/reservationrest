﻿using System.Diagnostics.CodeAnalysis;
using Domain.Entities;
using Xunit;

namespace Domain.Tests.Domain.Entities.Entities
{
    [ExcludeFromCodeCoverage]
    public class DiskTests
    {
        private static readonly Disk Disk = new Disk
        {
            Instance = new Instance(),
            InstanceId = 1,
            DiskType = new DiskType(),
            DeviceName = "DeviceName",
            Boot = true,
            Size = 50,
            DataDisk = true,
            AutoDelete = true,
            DiskId = 1,
            DiskTypeId = 1,
            ImageId = 1,
            Letter = "Letter"
        };

        [Fact]
        public void GetDiskData()
        {
            Assert.NotNull(Disk.Instance);
            Assert.Equal(1, Disk.InstanceId);
            Assert.NotNull(Disk.DiskType);
            Assert.Equal("DeviceName", Disk.DeviceName);
            Assert.True(Disk.Boot);
            Assert.Equal(50, Disk.Size);
            Assert.True(Disk.DataDisk);
            Assert.True(Disk.AutoDelete);
            Assert.Equal(1, Disk.DiskId);
            Assert.Equal(1, Disk.DiskTypeId);
            Assert.Equal(1, Disk.ImageId);
            Assert.Equal("Letter", Disk.Letter);
        }
    }
}
