﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Domain.Entities;
using Xunit;

namespace Domain.Tests.Domain.Entities.Entities
{
    [ExcludeFromCodeCoverage]
    public class DiskTypeTests
    {
        private static readonly DiskType DiskType = new DiskType
        {
            Disk = new List<Disk>(),
            Description = "Description",
            Name = "Name",
            DiskTypeId = 1,
            MaxDiskSizeGb = 100
        };

        [Fact]
        public void GetDiskTypeData()
        {
            Assert.NotNull(DiskType.Disk);
            Assert.Equal("Description", DiskType.Description);
            Assert.Equal("Name", DiskType.Name);
            Assert.Equal(1, DiskType.DiskTypeId);
            Assert.Equal(100, DiskType.MaxDiskSizeGb);
        }
    }
}
