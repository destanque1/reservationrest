﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Domain.Entities;
using Xunit;

namespace Domain.Tests.Domain.Entities.Entities
{
    [ExcludeFromCodeCoverage]
    public class PzoneTests
    {
        private static readonly Pzone Pzone = new Pzone
        {
            Name = "Name",
            ZoneId = 1,
            DesktopProvisionPrimaryZone = new List<DesktopProvision>(),
            DesktopProvisionSecundaryZone = new List<DesktopProvision>(),
            RegionId = 1
        };

        [Fact]
        public void GetPzoneData()
        {
            Assert.Equal("Name", Pzone.Name);
            Assert.Equal(1, Pzone.ZoneId);
            Assert.NotNull(Pzone.DesktopProvisionSecundaryZone);
            Assert.NotNull(Pzone.DesktopProvisionPrimaryZone);
            Assert.Equal(1, Pzone.RegionId);
        }
    }
}
