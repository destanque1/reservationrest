﻿using System.Diagnostics.CodeAnalysis;
using Domain.Entities;
using Xunit;

namespace Domain.Tests.Domain.Entities.Entities
{
    [ExcludeFromCodeCoverage]
    public class ProvisionTests
    {
        private static readonly Provision Provision = new Provision
        {
            Active = true,
            DesktopProvision = new DesktopProvision(),
            OrganizationId = 1,
            ProvisionId = 1,
            DeleteReason = "DeleteReason",
            IntegrationId = 1
        };

        [Fact]
        public void GetProvisionData()
        {
            Assert.True(Provision.Active);
            Assert.NotNull(Provision.DesktopProvision);
            Assert.Equal(1, Provision.OrganizationId);
            Assert.Equal(1, Provision.ProvisionId);
            Assert.Equal("DeleteReason", Provision.DeleteReason);
            Assert.Equal(1, Provision.IntegrationId);
        }
    }
}
