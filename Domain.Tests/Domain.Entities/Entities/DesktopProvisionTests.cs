﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Domain.Entities;
using Xunit;

namespace Domain.Tests.Domain.Entities.Entities
{
    [ExcludeFromCodeCoverage]
    public class DesktopProvisionTests
    {
        private static readonly DesktopProvision  DesktopProvision = new DesktopProvision
        {
            Instance = new List<Instance>(),
            Provision = new Provision(),
            ProvisionId = 1,
            BigqueryDataset = "BigqueryDataset",
            DefaultLocalServerAdminPassword = "DefaultLocalServerAdminPassword",
            DefaultLocalServerAdminUser = "DefaultLocalServerAdminUser",
            DeploymentName = "DeploymentName",
            DnsName = "DnsName",
            ExternalDnsName = "ExternalDnsName",
            NetBiosName = "NetBiosName",
            OuName = "OuName",
            PrimaryZone = new Pzone(),
            PrimaryZoneId = 1,
            ProvisionTypeId = 1,
            SecundaryZone = new Pzone(),
            SecundaryZoneId = 1,
            Status = "Status",
            Suffix = "Suffix",
            TemplateId = 1,
            Version = 1
        };

        [Fact]
        public void GetDesktopProvisionData()
        {
            Assert.NotNull(DesktopProvision.Instance);
            Assert.NotNull(DesktopProvision.Provision);
            Assert.Equal(1, DesktopProvision.ProvisionId);
            Assert.Equal("BigqueryDataset", DesktopProvision.BigqueryDataset);
            Assert.Equal("DefaultLocalServerAdminPassword", DesktopProvision.DefaultLocalServerAdminPassword);
            Assert.Equal("DefaultLocalServerAdminUser", DesktopProvision.DefaultLocalServerAdminUser);
            Assert.Equal("DeploymentName", DesktopProvision.DeploymentName);
            Assert.Equal("DnsName", DesktopProvision.DnsName);
            Assert.Equal("ExternalDnsName", DesktopProvision.ExternalDnsName);
            Assert.Equal("NetBiosName", DesktopProvision.NetBiosName);
            Assert.Equal("OuName", DesktopProvision.OuName);
            Assert.NotNull(DesktopProvision.PrimaryZone);
            Assert.Equal(1, DesktopProvision.PrimaryZoneId);
            Assert.Equal(1, DesktopProvision.ProvisionTypeId);
            Assert.NotNull(DesktopProvision.SecundaryZone);
            Assert.Equal(1, DesktopProvision.SecundaryZoneId);
            Assert.Equal("Status", DesktopProvision.Status);
            Assert.Equal("Suffix", DesktopProvision.Suffix);
            Assert.Equal(1, DesktopProvision.TemplateId);
            Assert.Equal(1, DesktopProvision.Version);
        }
    }
}
