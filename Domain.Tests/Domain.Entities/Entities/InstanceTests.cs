﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Domain.Entities;
using Xunit;

namespace Domain.Tests.Domain.Entities.Entities
{
    [ExcludeFromCodeCoverage]
    public class InstanceTests
    {
        private static readonly Instance Instance = new Instance
        {
            Disk = new List<Disk>(),
            Active = true,
            Provision = new DesktopProvision(),
            InstanceId = 1,
            ProvisionId = 1,
            ExternalIp = new List<ExternalIp>(),
            ServerType = new ServerType(),
            InternalIp = new List<InternalIp>(),
            Description = "Description",
            Name = "Name",
            Domain = true,
            MachineTypeId = 1,
            InstanceInstanceCredential = new List<InstanceInstanceCredential>(),
            CanIpForward = 1,
            CollectionId = 1,
            CpuPlatformId = 1,
            FamilyId = 1,
            GraphicCardId = 1,
            Imported = true,
            InstanceStatusId = 1,
            ScaleId = 1,
            ServerTypeId = 1,
            ZoneId = 1
        };

        [Fact]
        public void GetInstanceData()
        {
            Assert.NotNull(Instance.Disk);
            Assert.True(Instance.Active);
            Assert.NotNull(Instance.Provision);
            Assert.Equal(1, Instance.InstanceId);
            Assert.Equal(1, Instance.ProvisionId);
            Assert.NotNull(Instance.ExternalIp);
            Assert.NotNull(Instance.ServerType);
            Assert.NotNull(Instance.InternalIp);
            Assert.Equal("Description", Instance.Description);
            Assert.Equal("Name", Instance.Name);
            Assert.True(Instance.Domain);
            Assert.Equal(1, Instance.MachineTypeId);
            Assert.NotNull(Instance.InstanceInstanceCredential);
            Assert.Equal(1, Instance.CanIpForward);
            Assert.Equal(1, Instance.CollectionId);
            Assert.Equal(1, Instance.CpuPlatformId);
            Assert.Equal(1, Instance.FamilyId);
            Assert.Equal(1, Instance.GraphicCardId);
            Assert.True(Instance.Imported);
            Assert.Equal(1, Instance.InstanceStatusId);
            Assert.Equal(1, Instance.ScaleId);
            Assert.Equal(1, Instance.ServerTypeId);
            Assert.Equal(1, Instance.ZoneId);
        }
    }
}
