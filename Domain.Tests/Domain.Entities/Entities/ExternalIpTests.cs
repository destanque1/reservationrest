﻿using System.Diagnostics.CodeAnalysis;
using Domain.Entities;
using Xunit;

namespace Domain.Tests.Domain.Entities.Entities
{
    [ExcludeFromCodeCoverage]
    public class ExternalIpTests
    {
        private static readonly ExternalIp ExternalIp = new ExternalIp
        {
            Instance = new Instance(),
            InstanceId = 1,
            IpAddress = "IpAddress",
            ExternalIpId = 1
        };

        [Fact]
        public void GetExternalIpData()
        {
            Assert.NotNull(ExternalIp.Instance);
            Assert.Equal(1, ExternalIp.InstanceId);
            Assert.Equal("IpAddress", ExternalIp.IpAddress);
            Assert.Equal(1, ExternalIp.ExternalIpId);
        }
    }
}
