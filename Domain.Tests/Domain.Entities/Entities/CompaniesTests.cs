﻿using System;
using System.Diagnostics.CodeAnalysis;
using Domain.Entities;
using Xunit;

namespace Domain.Tests.Domain.Entities.Entities
{
    [ExcludeFromCodeCoverage]
    public class CompaniesTests
    {
        private static Companies _company = new Companies
        {
            Active = true,
            CompanyId = 1,
            Code = "STR",
            Name = "Test",
            CompaniesStatusId = 3,
            ContactEmail = "jptest@gmail.com",
            ContactFirstName = "Casilla",
            ContactLastName = "Test",
            CreateDate = DateTime.Now,
            CwsclientId = "STR",
            DeleteReason = "Test",
            DeletedBy = "Juan",
            DeploymentStep = "win-config",
            EmailDomain = "asd.local",
            OnCloud = true,
            PhoneNumber = "6589652456",
            OrganizationId = 254,
            PartnerId = 568,
            PhoneNumberExt = "302",
            RemoveDate = DateTime.Now,
            SendToCloudDate = DateTime.Now,
            TimeZone = "Universal",
            UsersCount = 50,
            Website = "www.test.com"


        };
        [Fact]
        public static void SetGetCompanyData()
        {
            Assert.True(_company.Active);
            Assert.Equal(1,_company.CompanyId);
            Assert.Equal("STR", _company.Code);
            Assert.Equal("Test", _company.Name);
            Assert.Equal(3, _company.CompaniesStatusId);
            Assert.Equal("jptest@gmail.com",_company.ContactEmail);
            Assert.Equal("Casilla", _company.ContactFirstName);
            Assert.Equal("Test", _company.ContactLastName);
            Assert.IsType<DateTime>(_company.CreateDate);
            Assert.Equal("STR", _company.CwsclientId);
            Assert.Equal("Test", _company.DeleteReason);
            Assert.Equal("Juan", _company.DeletedBy);
            Assert.Equal("win-config", _company.DeploymentStep);
            Assert.Equal("asd.local", _company.EmailDomain);
            Assert.True(_company.OnCloud);
            Assert.Equal("6589652456", _company.PhoneNumber);
            Assert.Equal(254, _company.OrganizationId);
            Assert.Equal(568, _company.PartnerId);
            Assert.Equal("302", _company.PhoneNumberExt);
            Assert.IsType<DateTime>(_company.RemoveDate);
            Assert.IsType<DateTime>(_company.SendToCloudDate);
            Assert.Equal("Universal", _company.TimeZone);
            Assert.Equal(50, _company.UsersCount);
            Assert.Equal("www.test.com", _company.Website);

        }

    }
}
