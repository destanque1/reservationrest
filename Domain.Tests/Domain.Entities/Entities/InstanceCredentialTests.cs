﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Domain.Entities;
using Xunit;

namespace Domain.Tests.Domain.Entities.Entities
{
    [ExcludeFromCodeCoverage]
    public class InstanceCredentialTests
    {
        private static readonly InstanceCredential InstanceCredential = new InstanceCredential
        {
            ProvisionId = 1,
            Domain = "Domain",
            Username = "UserName",
            Password = "Password",
            PublicKey = "PublicKey",
            InstanceCredentialId = 1,
            InstanceInstanceCredential = new List<InstanceInstanceCredential>()
        };

        [Fact]
        public void GetInstanceCredentialData()
        {
            Assert.Equal(1, InstanceCredential.ProvisionId);
            Assert.Equal("Domain", InstanceCredential.Domain);
            Assert.Equal("UserName", InstanceCredential.Username);
            Assert.Equal("Password", InstanceCredential.Password);
            Assert.Equal("PublicKey", InstanceCredential.PublicKey);
            Assert.Equal(1, InstanceCredential.InstanceCredentialId);
            Assert.NotNull(InstanceCredential.InstanceInstanceCredential);
        }
    }
}
