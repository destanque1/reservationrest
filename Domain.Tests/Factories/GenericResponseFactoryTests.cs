﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using CrossCutting.Exceptions;
using Domain.Entities.ValueObjects;
using Domain.Factories;
using Xunit;

namespace Domain.Tests.Factories
{
    [ExcludeFromCodeCoverage]
    public class GenericResponseFactoryTests
    {
        
        [Fact]
        public static void CreateWithCode()
        {
            var result =  GenericResponseFactory<string>.Create(1, "Generic Response");
            Assert.IsType<GenericResponse<string>>(result);
            Assert.IsType<int>(result.Code);
        }
        [Fact]
        public static void CreateWithMessage()
        {
            var result =  GenericResponseFactory<string>.Create(1,  "Invalid Data", "CasServices");
            Assert.IsType<GenericResponse<string>>(result);
            Assert.IsType<int>(result.Code);
            Assert.Equal("CasServices", result.Value);
            Assert.Equal("Invalid Data", result.Errors[0].Message);
        }


        [Fact]
        public static void CreateWithSingleError()
        {
            var result = GenericResponseFactory<string>.Create(1, new SingleError { Location = "CasServices", Message = "Invalid Deployment" }, "Single Error");
            Assert.IsType<GenericResponse<string>>(result);
            Assert.IsType<int>(result.Code);
            Assert.IsType<SingleError>(result.Errors[0]);
            Assert.Equal("Invalid Deployment", result.Errors[0].Message);
            Assert.Equal("CasServices", result.Errors[0].Location);
            Assert.Equal("Single Error", result.Value);
        }
        [Fact]
        public static void CreateWithListOfSingleError()
        {
            var result = GenericResponseFactory<string>.Create(1, new List<SingleError> { new SingleError { Location = "CasServices", Message = "Invalid Deployment" } }, "List of Errors");
            Assert.IsType<GenericResponse<string>>(result);
            Assert.IsType<int>(result.Code);
            Assert.IsType<List<SingleError>>(result.Errors);
            Assert.Equal("Invalid Deployment", result.Errors[0].Message);
            Assert.Equal("CasServices", result.Errors[0].Location);
            Assert.Equal("List of Errors", result.Value);
        }

        [Fact]
        public static void Compare()
        {
            var result = GenericResponseFactory<string>.Create(1, new List<SingleError> { new SingleError { Location = "CasServices", Message = "Invalid Deployment" } }, "List of Errors");
            var copy = result;
            var equal = result.Equals(copy);
            Assert.True(equal);
        }
        
    }

}
