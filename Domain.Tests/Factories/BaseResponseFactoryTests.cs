﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using CrossCutting.Exceptions;
using Domain.Entities.ValueObjects;
using Domain.Factories;
using Xunit;

namespace Domain.Tests.Factories
{
    [ExcludeFromCodeCoverage]
    public class BaseResponseFactoryTests
    {
        [Fact]
        public void CreateWithCode()
        {
            var result =  BaseResponseFactory.Create(1);
            Assert.IsType<BaseResponse>(result);
            Assert.IsType<int>(result.Code);
        }
        [Fact]
        public void CreateWithMessage()
        {
            var result =  BaseResponseFactory.Create(1, "Incorrect Code");
            Assert.IsType<BaseResponse>(result);
            Assert.IsType<int>(result.Code);
            Assert.Equal("Incorrect Code", result.Errors[0].Message);
            Assert.Equal("CasServices", result.Errors[0].Location);
        }
        [Fact]
        public void CreateWithMessageAndLocation()
        {
            var result =  BaseResponseFactory.Create(1, "Incorrect Code", "CasBaseResponseFactory");
            Assert.IsType<BaseResponse>(result);
            Assert.IsType<int>(result.Code);
            Assert.Equal("Incorrect Code", result.Errors[0].Message);
            Assert.Equal("CasBaseResponseFactory", result.Errors[0].Location);
        }
        [Fact]
        public void CreateWithSingleError()
        {
            var result =  BaseResponseFactory.Create(1, new SingleError{Location = "CasServices",Message = "Invalid Deployment"});
            Assert.IsType<BaseResponse>(result);
            Assert.IsType<int>(result.Code);
            Assert.IsType<SingleError>(result.Errors[0]);
            Assert.Equal("Invalid Deployment", result.Errors[0].Message);
            Assert.Equal("CasServices", result.Errors[0].Location);
        }
        [Fact]
        public void CreateWithListOfSingleError()
        {
            var result =  BaseResponseFactory.Create(1, new List<SingleError>{new SingleError{Location = "CasServices",Message = "Invalid Deployment"}});
            Assert.IsType<BaseResponse>(result);
            Assert.IsType<int>(result.Code);
            Assert.IsType<List<SingleError>>(result.Errors);
            Assert.Equal("Invalid Deployment", result.Errors[0].Message);
            Assert.Equal("CasServices", result.Errors[0].Location);
        }

        [Fact]
        public void CompareEquals()
        {
            var result = BaseResponseFactory.Create(1, new List<SingleError> { new SingleError { Location = "CasServices", Message = "Invalid Deployment" } });
            var other = result;

            var equal = result.Equals(other);
            Assert.True(equal);
        }

        [Fact]
        public void CompareNotEquals()
        {
            var result = BaseResponseFactory.Create(1, new List<SingleError> { new SingleError { Location = "CasServices", Message = "Invalid Deployment" } });
            var other = BaseResponseFactory.Create(1, new List<SingleError> { new SingleError { Location = "CasServices", Message = "Invalid Deployment" } });

            var equal = result != other;
            Assert.True(equal);
        }

    }
}
