﻿using System.Diagnostics.CodeAnalysis;
using System.Threading;

namespace Domain.Tests.Factories
{
    [ExcludeFromCodeCoverage]
    internal class PropertyJson
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }


        public PropertyJson(string name, string value, string type)
        {
            Name = name;
            Value = value;
            Type = type;
        }

        public PropertyJson()
        {
           
        }
    }
}