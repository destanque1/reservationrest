﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;

namespace Domain.Tests.Factories
{
    [ExcludeFromCodeCoverage]
    internal class ErrorJson
    {
        public int Id { get; set; }
        public string Location { get; set; }
        public string Message { get; set; }
        public string Code { get; set; }
        public List<string> Errors { get; set; }
        public List<string> Value { get; set; }


        public ErrorJson(int id, string location, string message, string code, List<string> errors, List<string> value)
        {
            Id = id;
            Location = location;
            Message = message;
            Code = code;
            Errors = errors;
            Value = value;
        }

        public ErrorJson()
        {
           
        }
    }
}