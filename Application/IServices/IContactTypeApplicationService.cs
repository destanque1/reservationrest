﻿using System.Threading.Tasks;
using Application.Dtos;
using CrossCutting.Dtos;
using CrossCutting.Filters;
using Domain.Entities.ValueObjects;
using Microsoft.AspNetCore.Mvc;

namespace Application.IServices
{
    public interface IContactTypeApplicationService
    {
        Task<ActionResult<ListDto<ContactTypeDto>>> GetContactTypesAsync(int pageIndex = 1, int pageSize = 200, string order = "", bool ascending = true);
        Task<ActionResult<ListDto<ContactTypeDto>>> SearchContactTypesAsync(SearchParam options);
        Task<ActionResult<ContactTypeDto>> GetContactTypeAsync(int contactTypeId);
        Task<ActionResult<ContactTypeModel>> CreateContactTypeAsync(ContactTypeModel contactTypeModel);
        Task<ActionResult<ContactTypeModel>> UpdateContactTypeAsync(int contactTypeId, ContactTypeModel contactTypeModel);
        Task<ActionResult<bool>> DeleteContactTypeAsync(int contactTypeId);
    }
}
