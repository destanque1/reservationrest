﻿using System.Threading.Tasks;
using Application.Dtos;
using CrossCutting.Dtos;
using CrossCutting.Filters;
using Domain.Entities.ValueObjects;
using Microsoft.AspNetCore.Mvc;

namespace Application.IServices
{
    public interface IReservationApplicationService
    {
        Task<ActionResult<ListDto<ReservationDto>>> GetReservationsAsync(int pageIndex = 1, int pageSize = 200, string order = "", bool ascending = true);
        Task<ActionResult<ListDto<ReservationDto>>> SearchReservationsAsync(SearchParam options);
        Task<ActionResult<ReservationDto>> GetReservationAsync(int reservationId);
        Task<ActionResult<ReservationModel>> CreateReservationAsync(ReservationModel reservationModel);
        Task<ActionResult<ReservationModel>> UpdateReservationAsync(int reservationId, ReservationModel reservationModel);
        Task<ActionResult<bool>> DeleteReservationAsync(int reservationId);
        Task<ActionResult<int>> UpdateReservationStatusByStoreProcedureAsync();

    }
}
