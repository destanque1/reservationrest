﻿using System.Threading.Tasks;
using Application.Dtos;
using CrossCutting.Dtos;
using CrossCutting.Filters;
using Domain.Entities.ValueObjects;
using Microsoft.AspNetCore.Mvc;
using ContactModelAdapter = Domain.Entities.ValueObjects.Adapter.ContactModelAdapter;

namespace Application.IServices
{
    public interface IContactApplicationService
    {
        Task<ActionResult<ListDto<ContactDto>>> GetContactsAsync(int pageIndex = 1, int pageSize = 200, string order = "", bool ascending = true);
        Task<ActionResult<ListDto<ContactDto>>> SearchContactsAsync(SearchParam options);
        Task<ActionResult<ContactDto>> GetContactAsync(int contactId);
        Task<ActionResult<ContactModel>> CreateContactAsync(ContactModel contactModel);
        Task<ActionResult<ContactModel>> UpdateContactAsync(int contactId, ContactModel contactModel);
        Task<ActionResult<bool>> DeleteContactAsync(int contactId);
      
    }
}
