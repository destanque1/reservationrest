﻿using System;
using System.Threading.Tasks;
using Application.IServices;
using Microsoft.Extensions.DependencyInjection;
using Quartz;

namespace Application.Jobs
{
   
    [DisallowConcurrentExecution]
    public class UpdateReservationStatusJob : IJob
    {
        private readonly IServiceProvider _provider;
        public UpdateReservationStatusJob(IServiceProvider provider)
        {
            _provider = provider;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            using var scope = _provider.CreateScope();
            // Resolve the Scoped service
            var service = scope.ServiceProvider.GetService<IReservationApplicationService>();
            await service.UpdateReservationStatusByStoreProcedureAsync();
           
        }
    }
}
