﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using Application.Dtos;
using Application.Dtos.Adapters;
using Application.IServices;
using CrossCutting.Adapters;
using CrossCutting.Dtos;
using CrossCutting.Exceptions;
using CrossCutting.Filters;
using CrossCutting.Order;
using Domain.Entities;
using Domain.Entities.Conts;
using Domain.Entities.Validators;
using Domain.Entities.ValueObjects;
using Domain.Entities.ValueObjects.Adapter;
using Domain.IRepositories;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Application.Services
{
    public class ReservationApplicationService: IReservationApplicationService
    {
        #region Private (Db access and other services)

        private readonly IUnitOfWork _unitOfWork;

        public ReservationApplicationService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        /// <summary>
        /// Get reservation list and request order
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="order"></param>
        /// <param name="ascending"></param>
        /// <returns>Returns all entities that matches all conditions</returns>
        public async Task<ActionResult<ListDto<ReservationDto>>> GetReservationsAsync(int pageIndex = 1, int pageSize = 200, string order = "", bool ascending = true)
        {
            var orderExpression = OrderBuilder.GetExpression<Reservation>(order) ?? (x => x.ReservationId);
            var includeRel = new Expression<Func<Reservation, object>>[] { x => x.ReservationStatus, x => x.Contact }; 
            Func<IQueryable<Reservation>, IOrderedQueryable<Reservation>> orderBy = x => ascending ? x.OrderBy(orderExpression) : x.OrderByDescending(orderExpression);
            var reservations = await _unitOfWork.ReservationRepository.GetQueryBy(sortFilter: orderBy, pageIndex: pageIndex,
                pageSize: pageSize, include: includeRel).Select(i => ReservationDtoAdapter.Create(i)).ToListAsync();
            var count = await _unitOfWork.ReservationRepository.CountAsync();

            return ListDtoAdapter<ReservationDto>.Create(count, reservations);
        }

        /// <summary>
        /// Search with custom queries and obtaining reservation list
        /// </summary>
        /// <param name="options"></param>
        /// <returns>Returns all entities that matches all conditions</returns>
        public Task<ActionResult<ListDto<ReservationDto>>> SearchReservationsAsync(SearchParam options)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Responsible for obtaining a reservation
        /// </summary>
        /// <param name="reservationId"></param>
        /// <returns>Returns ContactDto entity </returns>
        public async Task<ActionResult<ReservationDto>> GetReservationAsync(int reservationId)
        {
            if (reservationId <= 0)
                return new NotFoundObjectResult(ErrorAdapter.Create(LocationConsts.Reservation, Messages.ReservationNotFound));
            var includeRel = new Expression<Func<Reservation, object>>[] { x => x.ReservationStatus, x => x.Contact };
            var reservation = await _unitOfWork.ReservationRepository.GetOneByAsync(filter: x => x.ReservationId == reservationId, include: includeRel);
            if(reservation == null)
                return new NotFoundObjectResult(ErrorAdapter.Create(LocationConsts.Reservation, Messages.ReservationNotFound));
            return ReservationDtoAdapter.Create(reservation);
        }

        /// <summary>
        /// Responsible for creating a reservation
        /// </summary>
        /// <param name="reservationModel"></param>
        /// <returns>Returns the same input entity</returns>
        public async Task<ActionResult<ReservationModel>> CreateReservationAsync(ReservationModel reservationModel)
        {
            if (reservationModel == null)
                throw new CustomException(LocationConsts.Reservation, Messages.NotNullModel, HttpStatusCode.BadRequest, (int)HttpStatusCode.BadRequest);
            //Validate metadata model
            var validator = new ReservationModelValidator();
            validator.ValidateAndThrow(reservationModel);
            //Convert model to entity
            reservationModel.ReservationId = 0;
            var reservation = ReservationModelAdapter.Create(reservationModel);
            //Validate domain
            var existReservation = await _unitOfWork.ReservationRepository.AnyAsync(filter: x => x.Date == reservationModel.Date && x.Contact.ContactId == reservationModel.ContactId);
            if (existReservation)
                throw new CustomException(LocationConsts.Reservation, Messages.ReservationContactAlreadyExist, HttpStatusCode.BadRequest, (int)HttpStatusCode.BadRequest);
            var contact = await _unitOfWork.ContactRepository.GetOneByAsync(filter: x => x.ContactId == reservationModel.ContactId, tracking: true);
            if (contact == null)
                return new NotFoundObjectResult(ErrorAdapter.Create(LocationConsts.Reservation, Messages.ContactNotFound));
            var status = await _unitOfWork.ReservationStatusRepository.GetOneByAsync(filter: x => x.Code.ToLower() == "ptc", tracking: true);
            if (status == null)
                return new NotFoundObjectResult(ErrorAdapter.Create(LocationConsts.Reservation, Messages.ReservationStatusNotFound));
            reservation.Contact = contact;
            reservation.ReservationStatus = status;
            //Persist
            _unitOfWork.ReservationRepository.Create(reservation);
            await _unitOfWork.CommitAsync();
            return ReservationModelAdapter.Create(reservation);
        }

        /// <summary>
        /// Responsible for updating a reservation 
        /// </summary>
        /// <param name="reservationId"></param>
        /// <param name="reservationModel"></param>
        /// <returns>Returns the same input entity</returns>
        public async Task<ActionResult<ReservationModel>> UpdateReservationAsync(int reservationId, ReservationModel reservationModel)
        {
            if (reservationId <= 0)
                return new NotFoundObjectResult(ErrorAdapter.Create(LocationConsts.Reservation, Messages.ReservationNotFound));
            if (reservationModel == null)
                throw new CustomException(LocationConsts.Reservation, Messages.NotNullModel, HttpStatusCode.BadRequest, (int)HttpStatusCode.BadRequest);
            //Validate metadata model
            reservationModel.ReservationId = reservationId;
            var validator = new ReservationModelValidator();
            validator.ValidateAndThrow(reservationModel);
            //Find entity
            var includeRel = new Expression<Func<Reservation, object>>[] { x => x.ReservationStatus, x => x.Contact };
            var reservation = await _unitOfWork.ReservationRepository.GetOneByAsync(filter: x => x.ReservationId == reservationModel.ReservationId, tracking: true, include: includeRel);
            if (reservation == null)
                return new NotFoundObjectResult(ErrorAdapter.Create(LocationConsts.Reservation, Messages.ReservationNotFound));
            //Validate domain
            if(reservation.Contact.ContactId != reservationModel.ContactId)
                throw new CustomException(LocationConsts.Reservation, Messages.ReservationContactNotChange, HttpStatusCode.BadRequest, (int)HttpStatusCode.BadRequest);
            var existReservation = await _unitOfWork.ReservationRepository.AnyAsync(filter: x => x.Date == reservationModel.Date && x.Contact.ContactId == reservationModel.ContactId && x.ReservationId != reservationModel.ReservationId);
            if (existReservation)
                throw new CustomException(LocationConsts.Reservation, Messages.ReservationContactAlreadyExist, HttpStatusCode.BadRequest, (int)HttpStatusCode.BadRequest);
            reservationModel.StatusId = reservationModel.StatusId > 0 ? reservationModel.StatusId : reservation.ReservationStatus.ReservationStatusId;
             var status = await _unitOfWork.ReservationStatusRepository.GetOneByAsync(filter: x => x.ReservationStatusId == reservationModel.StatusId, tracking: true);
            if (status == null)
                return new NotFoundObjectResult(ErrorAdapter.Create(LocationConsts.Reservation, Messages.ReservationStatusNotFound));
            //Change values
            reservation.Description = reservationModel.Description;
            reservation.Date = reservationModel.Date;
            reservation.ReservationStatus = status;
            reservation.Ranking = reservationModel.Ranking;
            //Persist
            _unitOfWork.ReservationRepository.Update(reservation);
            await _unitOfWork.CommitAsync();

            return ReservationModelAdapter.Create(reservation);
        }

        /// <summary>
        /// Responsible for deleting a reservation
        /// </summary>
        /// <param name="reservationId"></param>
        /// <returns>Returns a boolean value</returns>
        public async Task<ActionResult<bool>> DeleteReservationAsync(int reservationId)
        {
            if (reservationId <= 0)
                return new NotFoundObjectResult(ErrorAdapter.Create(LocationConsts.Reservation, Messages.ReservationNotFound));
            await _unitOfWork.ReservationRepository.DeleteAsync(filter: x => x.ReservationId == reservationId);
            await _unitOfWork.CommitAsync();
            return true;
        }


        /// <summary>
        /// Responsible for run store procedure to update status of reservations
        /// </summary>
        /// <returns>Returns a boolean value</returns>
        public async Task<ActionResult<int>> UpdateReservationStatusByStoreProcedureAsync()
        {
             var result = await _unitOfWork.ReservationRepository.ExecStoreProcedure(LocationConsts.UpdateReservationStatus);
            await _unitOfWork.CommitAsync();
            return result;
        }
    }
}
