﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Application.Dtos;
using Application.Dtos.Adapters;
using Application.IServices;
using CrossCutting.Adapters;
using CrossCutting.Dtos;
using CrossCutting.Exceptions;
using CrossCutting.Filters;
using CrossCutting.Order;
using Domain.Entities;
using Domain.Entities.Conts;
using Domain.Entities.Validators;
using Domain.Entities.ValueObjects;
using Domain.Entities.ValueObjects.Adapter;
using Domain.IRepositories;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace Application.Services
{
    public class ContactApplicationService : IContactApplicationService
    {
        #region Private (Db access and other services)

        private readonly IUnitOfWork _unitOfWork;

        public ContactApplicationService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        /// <summary>
        /// Get contact list and request order
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="order"></param>
        /// <param name="ascending"></param>
        /// <returns>Returns all entities that matches all conditions</returns>
        public async Task<ActionResult<ListDto<ContactDto>>> GetContactsAsync(int pageIndex = 1, int pageSize = 200, string order = "", bool ascending = true)
        {
            var orderExpression = OrderBuilder.GetExpression<Contact>(order) ?? (x => x.ContactId);
            Func<IQueryable<Contact>, IOrderedQueryable<Contact>> orderBy = x => ascending ? x.OrderBy(orderExpression) : x.OrderByDescending(orderExpression);
            var contacts = await _unitOfWork.ContactRepository.GetQueryBy(sortFilter: orderBy, pageIndex: pageIndex,
                pageSize: pageSize, include: x => x.ContactType).Select(i => ContactDtoAdapter.Create(i)).ToListAsync();
            var countTask = await _unitOfWork.ContactRepository.CountAsync();
            return ListDtoAdapter<ContactDto>.Create(countTask, contacts);
        }

        /// <summary>
        /// Search with custom queries and obtaining contacts list
        /// </summary>
        /// <param name="options"></param>
        /// <returns>Returns all entities that matches all conditions</returns>
        public Task<ActionResult<ListDto<ContactDto>>> SearchContactsAsync(SearchParam options)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Responsible for obtaining a contact
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns>Returns ContactDto entity </returns>
        public async Task<ActionResult<ContactDto>> GetContactAsync(int contactId)
        {
            if (contactId <= 0)
                return new NotFoundObjectResult(ErrorAdapter.Create(LocationConsts.Contact, Messages.ContactNotFound));
            var contact = await _unitOfWork.ContactRepository.GetOneByAsync(filter:x=> x.ContactId == contactId, include: x => x.ContactType);
            if(contact == null)
                return new NotFoundObjectResult(ErrorAdapter.Create(LocationConsts.Contact, Messages.ContactNotFound));
            return ContactDtoAdapter.Create(contact);
        }

        /// <summary>
        /// Responsible for creating a contact
        /// </summary>
        /// <param name="contactModel"></param>
        /// <returns>Returns the same input entity</returns>
        public async Task<ActionResult<ContactModel>> CreateContactAsync(ContactModel contactModel)
        {
            if (contactModel == null)
                throw new CustomException(LocationConsts.Contact, Messages.NotNullModel, HttpStatusCode.BadRequest, (int)HttpStatusCode.BadRequest);
            //Validate metadata model
            var validator = new ContactModelValidator();
            validator.ValidateAndThrow(contactModel);
            //Convert model to entity
            contactModel.ContactId = 0;
            var contact = ContactModelAdapter.Create(contactModel);
            //Validate domain
            var existContact = await _unitOfWork.ContactRepository.AnyAsync(filter: x => x.Name.ToLower() == contactModel.Name.ToLower());
            if(existContact)
                throw new CustomException(LocationConsts.Contact, Messages.ContactNameAlreadyExist, HttpStatusCode.BadRequest, (int)HttpStatusCode.BadRequest);
            var contactType = await _unitOfWork.ContactTypeRepository.GetOneByAsync(filter: x => x.ContactTypeId == contactModel.ContactTypeId, tracking: true);
            if (contactType == null)
                return new NotFoundObjectResult(ErrorAdapter.Create(LocationConsts.Contact, Messages.ContactTypeNotFound));
            contact.ContactType = contactType;
            //Persist
            _unitOfWork.ContactRepository.Create(contact);
            await _unitOfWork.CommitAsync();

            return ContactModelAdapter.Create(contact);

        }

        /// <summary>
        /// Responsible for updating a contact 
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="contactModel"></param>
        /// <returns>Returns the same input entity</returns>
        public async Task<ActionResult<ContactModel>> UpdateContactAsync(int contactId, ContactModel contactModel)
        {
            if (contactId <= 0)
                return new NotFoundObjectResult(ErrorAdapter.Create(LocationConsts.Contact, Messages.ContactNotFound));
            if (contactModel == null)
                throw new CustomException(LocationConsts.Contact, Messages.NotNullModel, HttpStatusCode.BadRequest, (int)HttpStatusCode.BadRequest);
            //Validate metadata model
            contactModel.ContactId = contactId;
            var validator = new ContactModelValidator();
            validator.ValidateAndThrow(contactModel);
            //Find entity
            var contact = await _unitOfWork.ContactRepository.GetOneByAsync(filter: x => x.ContactId == contactModel.ContactId, tracking: true);
            if (contact == null)
                return new NotFoundObjectResult(ErrorAdapter.Create(LocationConsts.Contact, Messages.ContactTypeNotFound));
            //Validate domain
            var existContact = await _unitOfWork.ContactRepository.AnyAsync(filter: x => x.Name.ToLower() == contactModel.Name.ToLower() && x.ContactId != contactId);
            if (existContact)
                throw new CustomException(LocationConsts.Contact, Messages.ContactNameAlreadyExist, HttpStatusCode.BadRequest, (int)HttpStatusCode.BadRequest);
            var contactType = await _unitOfWork.ContactTypeRepository.GetOneByAsync(filter: x => x.ContactTypeId == contactModel.ContactTypeId, tracking: true);
            if (contactType == null)
                return new NotFoundObjectResult(ErrorAdapter.Create(LocationConsts.Contact, Messages.ContactTypeNotFound));
            //Change values
            contact.ContactType = contactType;
            contact.Name = contactModel.Name;
            contact.BirthDate = contactModel.BirthDate;
            contact.Phone = contactModel.Phone;
            //Persist
            _unitOfWork.ContactRepository.Update(contact);
            await _unitOfWork.CommitAsync();

            return ContactModelAdapter.Create(contact);
        }

        /// <summary>
        /// Responsible for deleting a contact
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns>Returns a boolean value</returns>
        public async Task<ActionResult<bool>> DeleteContactAsync(int contactId)
        {
            if (contactId <= 0)
                return new NotFoundObjectResult(ErrorAdapter.Create(LocationConsts.Contact, Messages.ContactNotFound));
            //Validate domain
            var existInReservationContact = await _unitOfWork.ReservationRepository.AnyAsync(filter: x => x.Contact.ContactId == contactId && x.ReservationStatus.Code.ToLower() != "clo" );
            if (existInReservationContact)
                throw new CustomException(LocationConsts.Contact, Messages.ContactInReservation, HttpStatusCode.BadRequest, (int)HttpStatusCode.BadRequest);

            await _unitOfWork.ReservationRepository.DeleteAsync(filter: x => x.Contact.ContactId == contactId);
            await _unitOfWork.ContactRepository.DeleteAsync(filter: x => x.ContactId == contactId);
            await _unitOfWork.CommitAsync();
            return true;
        }
    }
}
