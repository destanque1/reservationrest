﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Application.Dtos;
using Application.Dtos.Adapters;
using Application.IServices;
using CrossCutting.Adapters;
using CrossCutting.Dtos;
using CrossCutting.Filters;
using CrossCutting.Order;
using Domain.Entities;
using Domain.Entities.Conts;
using Domain.Entities.ValueObjects;
using Domain.IRepositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Application.Services
{
    public class ContactTypeApplicationService : IContactTypeApplicationService
    {
        #region Private (Db access and other services)

        private readonly IUnitOfWork _unitOfWork;

        public ContactTypeApplicationService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        /// <summary>
        /// Get contact types list and request order
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="order"></param>
        /// <param name="ascending"></param>
        /// <returns>Returns all entities that matches all conditions</returns>
        public async Task<ActionResult<ListDto<ContactTypeDto>>> GetContactTypesAsync(int pageIndex = 1, int pageSize = 200, string order = "", bool ascending = true)
        {
            var orderExpression = OrderBuilder.GetExpression<ContactType>(order) ?? (x => x.ContactTypeId);
            Func<IQueryable<ContactType>, IOrderedQueryable<ContactType>> orderBy =  x => ascending ? x.OrderBy(orderExpression) : x.OrderByDescending(orderExpression);
            var contactTypes = await _unitOfWork.ContactTypeRepository.GetQueryBy(sortFilter: orderBy, pageIndex:pageIndex,
                    pageSize:pageSize).Select(i => ContactTypeDtoAdapter.Create(i)).ToListAsync();
            var countTask = await _unitOfWork.ContactTypeRepository.CountAsync();

            return ListDtoAdapter<ContactTypeDto>.Create( countTask, contactTypes);
        }

        /// <summary>
        /// Search with custom queries and obtaining contact types list
        /// </summary>
        /// <param name="options"></param>
        /// <returns>Returns all entities that matches all conditions</returns>
        public Task<ActionResult<ListDto<ContactTypeDto>>> SearchContactTypesAsync(SearchParam options)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Responsible for obtaining a contact type
        /// </summary>
        /// <param name="contactTypeId"></param>
        /// <returns>Returns ContactTypeDto entity </returns>
        public async Task<ActionResult<ContactTypeDto>> GetContactTypeAsync(int contactTypeId)
        {
            if (contactTypeId <= 0)
                return new NotFoundObjectResult(ErrorAdapter.Create(LocationConsts.ContactType, Messages.ContactTypeNotFound));
            var contactType = await _unitOfWork.ContactTypeRepository.GetOneByAsync(filter: x => x.ContactTypeId == contactTypeId);
            if (contactType == null)
                return new NotFoundObjectResult(ErrorAdapter.Create(LocationConsts.Contact, Messages.ContactNotFound));
            return ContactTypeDtoAdapter.Create(contactType);
        }

        /// <summary>
        /// Responsible for creating a contact type
        /// </summary>
        /// <param name="contactTypeModel"></param>
        /// <returns>Returns the same input entity</returns>
        public Task<ActionResult<ContactTypeModel>> CreateContactTypeAsync(ContactTypeModel contactTypeModel)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Responsible for updating a contact type
        /// </summary>
        /// <param name="contactTypeId"></param>
        /// <param name="contactTypeModel"></param>
        /// <returns>Returns the same input entity</returns>
        public Task<ActionResult<ContactTypeModel>> UpdateContactTypeAsync(int contactTypeId, ContactTypeModel contactTypeModel)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Responsible for deleting a contact type
        /// </summary>
        /// <param name="contactTypeId"></param>
        /// <returns>Returns a boolean value</returns>
        public async Task<ActionResult<bool>> DeleteContactTypeAsync(int contactTypeId)
        {
            if (contactTypeId <= 0)
                return new NotFoundObjectResult(ErrorAdapter.Create(LocationConsts.ContactType, Messages.ContactTypeNotFound));
            await _unitOfWork.ContactTypeRepository.DeleteAsync(filter: x => x.ContactTypeId == contactTypeId);
            await _unitOfWork.CommitAsync();
            return true;
        }
    }
}
