﻿using System.Diagnostics.CodeAnalysis;
using System.Net;
using CrossCutting.Consts;
using CrossCutting.Exceptions;
using Xunit;

namespace CrossCutting.Tests.Exceptions
{
    [ExcludeFromCodeCoverage]
    public class CustomExceptionTests
    {

        [Fact]
        public void RequiredParams()
        {
            var exception = new CustomException("CasExceptionTests", "cas exception test");
            Assert.IsAssignableFrom<CustomException>(exception);
        }

        [Fact]
        public void AllParams()
        {
            var exception = new CustomException("CasExceptionTests", "cas exception test", HttpStatusCode.Ambiguous, ResponseCodes.BadRequest);
            Assert.IsAssignableFrom<CustomException>(exception);
        }

        [Fact]
        public void StatusCode()
        {
            var exception = new CustomException("CasExceptionTests", "cas exception test", HttpStatusCode.InternalServerError, ResponseCodes.BadRequest);
            Assert.Equal(HttpStatusCode.InternalServerError, exception.StatusCode);
        }

        [Fact]
        public void Code()
        {
            var exception = new CustomException("CasExceptionTests", "cas exception test", HttpStatusCode.InternalServerError, ResponseCodes.BadRequest);
            Assert.Equal(ResponseCodes.BadRequest, exception.Code);
        }

        [Fact]
        public void Error()
        {
            var exception = new CustomException("CasExceptionTests", "cas exception test", HttpStatusCode.InternalServerError, ResponseCodes.BadRequest);
            Assert.IsAssignableFrom<SingleError>(exception.Error);
        }

    }
}
