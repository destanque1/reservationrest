﻿using System;
using System.Linq.Expressions;

namespace CrossCutting.Order
{
    public class OrderBuilder
    {
        /// <summary>
        /// Build Expression for Order
        /// </summary>
        /// <typeparam name="TEntity">Buisness Entity</typeparam>
        /// <param name="field">Order</param>
        /// <returns>Expression</returns>
        public static Expression<Func<TEntity, object>> GetExpression<TEntity>(string field)
        {
            if (string.IsNullOrEmpty(field))
                return null;

            var param = Expression.Parameter(typeof(TEntity), "t");

            //return
            return GetExpression<TEntity, object>(param, field);
        }


        /// <summary>
        /// Build Expression for Order
        /// </summary>
        /// <typeparam name="TEntity">Business Entity</typeparam>
        /// <typeparam name="TR">TKey</typeparam>
        /// <param name="param">Parameter Expression</param>
        /// <param name="field">Order</param>
        /// <returns>Expression</returns>
        private static Expression<Func<TEntity, TR>> GetExpression<TEntity, TR>(ParameterExpression param, string field) 
        {
            var props = field.Split('.');
            var type = typeof(TEntity);
            Expression expr = param;
            foreach (var prop in props)
            {
                // use reflection (not ComponentModel) to mirror LINQ
                var pi = type.GetProperty(prop);
                if (pi == null) continue;
                expr = Expression.Property(expr, pi);
                type = pi.PropertyType;
            }

            if (expr == param)
                return null;
            Expression conversion = Expression.Convert(expr, typeof(object));
            return (Expression<Func<TEntity, TR>>)Expression.Lambda(conversion, param);
        }
    }
}

