﻿using System;
using System.Runtime.Serialization;

namespace CrossCutting.Order
{
    [DataContract]
    public class SortOrder
    {
        [DataMember]
        public string ColumnName { get; set; }

        [DataMember]
        public bool Ascending { get; set; }

        public SortOrder()
        {
            ColumnName = String.Empty;
            Ascending = true;
        }
    }
}
