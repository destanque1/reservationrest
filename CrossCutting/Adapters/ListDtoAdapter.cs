﻿using System.Collections.Generic;
using CrossCutting.Dtos;

namespace CrossCutting.Adapters
{
    public static class ListDtoAdapter<T>
    {
        public static ListDto<T> Create(int count, List<T> items)
        {
            return  new ListDto<T>
            {
                Count = count,
                List = items
            };
        }
    }
}
