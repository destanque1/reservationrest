﻿using CrossCutting.Exceptions;

namespace CrossCutting.Adapters
{
    public static class ErrorAdapter
    {
        public static SingleError Create(string location, string message)
        {
            return new SingleError
            {
                Location = location,
                Message = message
            };
        }
    }
}
