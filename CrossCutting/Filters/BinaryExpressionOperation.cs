﻿

namespace CrossCutting.Filters
{
    public enum BinaryExpressionOperation
    {
        None,
        // Summary:
        //     Creates a System.Linq.Expressions.BinaryExpression that represents a bitwise
        //     AND operation.
        And,
        // Summary:
        //     Creates a System.Linq.Expressions.BinaryExpression that represents a conditional
        //     AND operation that evaluates the second operand only if the first operand
        //     evaluates to true.
        AndAlso,
        // Summary:
        //     Creates a System.Linq.Expressions.BinaryExpression that represents a bitwise
        //     OR operation.
        Or,
        // Summary:
        //     Creates a System.Linq.Expressions.BinaryExpression that represents a conditional
        //     OR operation that evaluates the second operand only if the first operand
        //     evaluates to false.
        OrElse,
        // Summary:
        //     Creates a System.Linq.Expressions.BinaryExpression that represents a bitwise
        //     XOR operation, using op_ExclusiveOr for user-defined types.
        ExclusiveOr,
        // Summary:
        //     Creates a System.Linq.Expressions.BinaryExpression that represents an assignment
        //     operation.
        Assign,
        // Summary:
        //     Creates a System.Linq.Expressions.BinaryExpression that represents an arithmetic
        //     addition operation that has overflow checking.
        AddChecked,
        // Summary:
        //     Creates a System.Linq.Expressions.BinaryExpression that represents an addition
        //     assignment operation that has overflow checking.
        AddAssignChecked,
        // Summary:
        //     Creates a System.Linq.Expressions.BinaryExpression that represents a coalescing
        //     operation.
        Coalesce,
        // Summary:
        //     Creates a System.Linq.Expressions.BinaryExpression that represents an arithmetic
        //     division operation.
        Divide,
        // Summary:
        //     Creates a System.Linq.Expressions.BinaryExpression that represents a bitwise
        //     left-shift operation.
        LeftShift,
        // Summary:
        //     Creates a System.Linq.Expressions.BinaryExpression that represents a bitwise
        //     right-shift operation.
        RightShift,
        // Summary:
        //     Creates a System.Linq.Expressions.BinaryExpression that represents an arithmetic
        //     remainder operation.
        Modulo
    }
}
