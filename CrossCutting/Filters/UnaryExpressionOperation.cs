﻿namespace CrossCutting.Filters
{
    public enum UnaryExpressionOperation
    {
        //Default
        None,
        // Summary:
        //     Creates a System.Linq.Expressions.UnaryExpression that represents an arithmetic
        //     negation operation.
        Negate,
        // Summary:
        //     Creates a System.Linq.Expressions.UnaryExpression that represents a bitwise
        //     complement operation.
        Not
    }
}
