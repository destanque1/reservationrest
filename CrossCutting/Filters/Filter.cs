﻿namespace CrossCutting.Filters
{
    public class Filter
    {
        public Filter()
        {
            this.ToLower = null;
        }
        public string PropertyName { get; set; }
        public PropertyOperation Operation { get; set; }
        public string Value { get; set; }
        public bool? ToLower { get; set; }
        //For Only Expression
        public UnaryExpressionOperation UnaryOperation { get; set; }
        //For Parent Expresesion
        public BinaryExpressionOperation BinaryOperation { get; set; }
    }
}
