﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using CrossCutting.Order;

namespace CrossCutting.Filters
{
    [DataContract]
    public class SearchParam
    {
        #region Private Member Region

        private int PIndex { get; set; }

        private int PSize { get; set; }

        #endregion

        [DataMember]
        public int PageIndex
        {
            get => PIndex != 0 ? PIndex : 1;
            set => PIndex = value;
        }

        [DataMember]
        public int PageSize
        {
            get => PSize != 0 && PSize <= 200 ? PSize : 25;
            set => PSize = value;
        }

        [DataMember]
        public List<Filter> Filter { get; set; }

        [DataMember]
        public SortOrder Order { get; set; }

        /// <summary>
        /// Search Param Constructor
        /// </summary>
        public SearchParam()
        {
            PageIndex = 1;
            PageSize = 25;
            Filter = null;
            Order = null;
        }
    }
}
