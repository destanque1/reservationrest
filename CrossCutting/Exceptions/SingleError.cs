﻿namespace CrossCutting.Exceptions
{
    public class SingleError
    {
        /// <summary>
        ///  Indicates the field in the request that caused the error.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Message { get; set; }
    }
}
