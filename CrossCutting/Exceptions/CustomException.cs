﻿using System;
using System.Net;
using CrossCutting.Consts;

namespace CrossCutting.Exceptions
{
    public class CustomException : Exception
    {
        public HttpStatusCode StatusCode { get; }

        public int Code { get; }

        public SingleError Error { get; }

        public CustomException(string location, string message, HttpStatusCode statusCode = HttpStatusCode.InternalServerError, int code = ResponseCodes.InternalServerError)
        {
            Error = new SingleError
            {
                Location = location,
                Message = message
            };
            StatusCode = statusCode;
            Code = code;
        }
    }
}
