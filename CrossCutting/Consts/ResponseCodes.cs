﻿namespace CrossCutting.Consts
{
    public static class ResponseCodes
    {
        public const int Done = 200;
        public const int DoneWithWarnings = 20007;
        public const int DoneRequireSchedule = 20008; 
        public const int NotModified = 304;
        public const int BadRequest = 400;
        public const int ValidationError = 40029;
        public const int AccessDenied = 401;
        public const int NotFound = 404;
        public const int InternalServerError = 500;
        public const int Timeout = 504;
        public const int UnprocessableEntity = 422;
   
    }
}
