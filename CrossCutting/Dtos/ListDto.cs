﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CrossCutting.Dtos
{
    public class ListDto<T>
    {
        [DataMember]
        public int Count { get; set; }

        [DataMember]
        public List<T> List { get; set; }
    }
}
