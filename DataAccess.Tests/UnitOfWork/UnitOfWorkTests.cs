﻿using System;
using System.Diagnostics.CodeAnalysis;
using Domain.Entities.Contexts;
using Domain.IRepositories;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace DataAccess.Tests.UnitOfWork
{
    [ExcludeFromCodeCoverage]
    public class UnitOfWorkTests: IDisposable
    {
        private IUnitOfWork _unitOfWork;

        public UnitOfWorkTests()
        {
            var options = new DbContextOptionsBuilder<CasContext>()
                .UseInMemoryDatabase($"UpdateTests-{Guid.NewGuid()}")
                .Options;

            var dbContext = new CasContext(options);
            _unitOfWork = new DataAccess.Repositories.UnitOfWork(dbContext);
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
        

        [Fact]
        public void DisposeUnitOfWork()
        {
            _unitOfWork.Dispose();
            Assert.True(true);
        }

        [Fact]
        public async void CommitAsync()
        {
            var result = await _unitOfWork.CommitAsync();
            Assert.InRange(result,0, 100);
        }

        [Fact]
        public void DiskRepository()
        {
            var result = _unitOfWork.DiskRepository;
            Assert.NotNull(result);
        }

        [Fact]
        public void DiskRepositoryInstanciated()
        {
            var result = _unitOfWork.DiskRepository;
            Assert.NotNull(result);
        }

        [Fact]
        public void InstanceRepository()
        {
            var result = _unitOfWork.InstanceRepository;
            Assert.NotNull(result);
        }

        [Fact]
        public void InstanceRepositoryInstanciated()
        {
            var result = _unitOfWork.InstanceRepository;
            Assert.NotNull(result);
        }

        [Fact]
        public void CompaniesRepository()
        {
            var result = _unitOfWork.CompaniesRepository;
            Assert.NotNull(result);
        }

        [Fact]
        public void CompaniesRepositoryInstanciated()
        {
            var result = _unitOfWork.CompaniesRepository;
            Assert.NotNull(result);
        }

        [Fact]
        public void ProvisionRepository()
        {
            var result = _unitOfWork.ProvisionRepository;
            Assert.NotNull(result);
        }

        [Fact]
        public void ProvisionRepositoryInstanciated()
        {
            var result = _unitOfWork.ProvisionRepository;
            Assert.NotNull(result);
        }

        [Fact]
        public void ExternalIpRepository()
        {
            var result = _unitOfWork.ExternalIpRepository;
            Assert.NotNull(result);
        }

        [Fact]
        public void ExternalIpRepositoryInstanciated()
        {
            var result = _unitOfWork.ExternalIpRepository;
            Assert.NotNull(result);
        }

        [Fact]
        public void InternalIpRepository()
        {
            var result = _unitOfWork.InternalIpRepository;
            Assert.NotNull(result);
        }

        [Fact]
        public void InternalIpRepositoryInstanciated()
        {
            var result = _unitOfWork.InternalIpRepository;
            Assert.NotNull(result);
        }

        [Fact]
        public void InstanceCredentialRepository()
        {
            var result = _unitOfWork.InstanceCredentialRepository;
            Assert.NotNull(result);
        }

        [Fact]
        public void InstanceCredentialRepositoryInstanciated()
        {
            var result = _unitOfWork.InstanceCredentialRepository;
            Assert.NotNull(result);
        }

        [Fact]
        public void DesktopProvisionRepository()
        {
            var result = _unitOfWork.DesktopProvisionRepository;
            Assert.NotNull(result);
        }

        [Fact]
        public void DesktopProvisionRepositoryInstanciated()
        {
            var result = _unitOfWork.DesktopProvisionRepository;
            Assert.NotNull(result);
        }

        [Fact]
        public void PzoneRepositoryRepository()
        {
            var result = _unitOfWork.PzoneRepository;
            Assert.NotNull(result);
        }

        [Fact]
        public void PzoneRepositoryRepositoryInstanciated()
        {
            var result = _unitOfWork.PzoneRepository;
            Assert.NotNull(result);
        }

    }
}
