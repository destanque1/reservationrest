﻿using System;
using System.Diagnostics.CodeAnalysis;
using Domain.Entities;
using Domain.Entities.Contexts;
using Domain.IRepositories;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace DataAccess.Tests.Repositories.CompaniesRepository
{
    [ExcludeFromCodeCoverage]
    public class CreateTests: IDisposable
    {
        private readonly CasContext _dbContext;
        private IGenericRepository<Companies> _repository;

        public CreateTests()
        {
            var options = new DbContextOptionsBuilder<CasContext>()
                .UseInMemoryDatabase("UpdateTests")
                .Options;

            _dbContext = new CasContext(options);
            _dbContext.Companies.Add(new Companies { Active = false });
            _dbContext.SaveChanges();
            _repository = new DataAccess.Repositories.CompaniesRepository(_dbContext);

        }

        public void Dispose()
        {
            _dbContext.Dispose();
            _repository = null;
        }
        

        [Fact]
        public void CreateNull()
        {
            try
            {
                _repository.Create(null);
                Assert.True(false);
            }
            catch (Exception)
            {
                Assert.True(true);
            }
            
        }

        [Fact]
        public void CreateSuccess()
        {
            _repository.Create(new Companies());
            Assert.True(true);
        }
    }
}
