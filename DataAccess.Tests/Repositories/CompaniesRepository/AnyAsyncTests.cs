﻿using System;
using System.Diagnostics.CodeAnalysis;
using Domain.Entities;
using Domain.Entities.Contexts;
using Domain.IRepositories;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace DataAccess.Tests.Repositories.CompaniesRepository
{
    [ExcludeFromCodeCoverage]
    public class AnyAsyncTests: IDisposable
    {
        private readonly CasContext _dbContext;
        private IGenericRepository<Companies> _repository;

        public AnyAsyncTests()
        {
            var options = new DbContextOptionsBuilder<CasContext>()
                .UseInMemoryDatabase("UpdateTests")
                .Options;

            _dbContext = new CasContext(options);
            _dbContext.Companies.Add(new Companies { Active = false });
            _dbContext.SaveChanges();
            _repository =new DataAccess.Repositories.CompaniesRepository(_dbContext);
           
        }

        public void Dispose()
        {
            _dbContext.Dispose();
            _repository = null;
        }
        

        [Fact]
        public async void AnyUnfiltered()
        {
            

            var result = _repository.AnyAsync();
            Assert.True(await result);

        }

        [Fact]
        public async void AnyWithFilter()
        {
            var result = _repository.AnyAsync(x => x.Active == false);
            Assert.True(await result);

        }


    }
}
