﻿using Domain.Entities;
using Domain.Entities.Contexts;
using Domain.IRepositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace DataAccess.Tests.Repositories.CompaniesRepository
{
    [ExcludeFromCodeCoverage]
    public class DeleteTests : IDisposable
    {
        private readonly CasContext _dbContext;
        private IGenericRepository<Companies> _repository;

        public DeleteTests()
        {
            var options = new DbContextOptionsBuilder<CasContext>()
                .UseInMemoryDatabase($"UpdateTests-{Guid.NewGuid()}")
                .Options;

            _dbContext = new CasContext(options);
            _dbContext.Companies.Add(new Companies { CompanyId = 5});
            _dbContext.SaveChanges();
            _repository = new DataAccess.Repositories.CompaniesRepository(_dbContext);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
            _repository = null;
        }

        [Fact]
        public void DeleteNull()
        {
            try
            {
                _repository.Delete(null);
            }
            catch (ArgumentNullException)
            {
                Assert.True(true);
            }
        }

        [Fact]
        public void DeleteEnity()
        {
            _repository.Delete(new Companies());
            Assert.True(true);
        }

        [Fact]
        public void DeleteByIdNotFound()
        {
            try
            {
                _repository.Delete(1);
            }
            catch (ArgumentNullException)
            {
                Assert.True(true);
            }
        }

        [Fact]
        public void DeleteByIdFound()
        {
            _repository.Delete(5);
            Assert.True(true);
        }

        [Fact]
        public async void DeleteAsyncNotFind()
        {
            await _repository.DeleteAsync(x => x.CompanyId == 1);
            Assert.True(true);
        }

        [Fact]
        public async void DeleteAsyncFind()
        {
            await _repository.DeleteAsync(x => x.CompanyId == 5);
            Assert.True(true);
        }

    }
}
