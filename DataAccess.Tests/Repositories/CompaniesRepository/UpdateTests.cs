﻿using System;
using System.Diagnostics.CodeAnalysis;
using Domain.Entities;
using Domain.Entities.Contexts;
using Domain.IRepositories;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace DataAccess.Tests.Repositories.CompaniesRepository
{
    [ExcludeFromCodeCoverage]
    public class UpdateTests : IDisposable
    {
        private readonly CasContext _dbContext;
        private IGenericRepository<Companies> _repository;

        public UpdateTests()
        {
            var options = new DbContextOptionsBuilder<CasContext>()
                .UseInMemoryDatabase($"UpdateTests-{Guid.NewGuid()}")
                .Options;

            _dbContext = new CasContext(options);
            _repository = new DataAccess.Repositories.CompaniesRepository(_dbContext);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
            _repository = null;
        }

        [Fact]
        public void UpdateNull()
        {
            try
            {
                _repository.Update(null);
            }
            catch (ArgumentNullException)
            {
                Assert.True(true);
            }
        }

        [Fact]
        public void UpdateSuccess()
        {
            try
            {
                _repository.Update(new Companies());
            }
            catch (ArgumentNullException)
            {
                Assert.True(true);
            }
        }
    }
}
