﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Domain.Entities;
using Domain.Entities.Contexts;
using Domain.IRepositories;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace DataAccess.Tests.Repositories.CompaniesRepository
{
    [ExcludeFromCodeCoverage]
    public class GetOneByAsyncTests: IDisposable
    {
        private readonly CasContext _dbContext;
        private IGenericRepository<Companies> _repository;

        public GetOneByAsyncTests()
        {
            var options = new DbContextOptionsBuilder<CasContext>()
                .UseInMemoryDatabase($"UpdateTests-{Guid.NewGuid()}")
                .Options;

            _dbContext = new CasContext(options);
            _dbContext.Companies.Add(new Companies { Active = false });
            _dbContext.SaveChanges();
            _repository = new DataAccess.Repositories.CompaniesRepository(_dbContext);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
            _repository = null;
        }
        
        [Fact]
        public async void FindOneNotExist()
        {
            var result = await _repository.GetOneByAsync(x => x.Active, x => x.OrderBy(y => y.Code));
            Assert.Null(result);
        }

        [Fact]
        public async void FindOneNotExistOrder()
        {
            var result = await _repository.GetOneByAsync(x => x.Active, x => x.OrderBy(y => y.Code));
            Assert.Null(result);
        }

        [Fact]
        public async void FindOneNotExistOrderNotTracking()
        {
            var result = await _repository.GetOneByAsync(x => x.Active, x => x.OrderBy(y => y.Code), true);
            Assert.Null(result);
        }

        [Fact]
        public async void FindOneExist()
        {
            var result = await _repository.GetOneByAsync(x => !x.Active);
            Assert.NotNull(result);
        }
    }
}
