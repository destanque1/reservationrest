﻿using System;
using System.Diagnostics.CodeAnalysis;
using Domain.Entities;
using Domain.Entities.Contexts;
using Domain.IRepositories;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace DataAccess.Tests.Repositories.CompaniesRepository
{
    [ExcludeFromCodeCoverage]
    public class CountAsyncTests: IDisposable
    {
        private readonly CasContext _dbContext;
        private IGenericRepository<Companies> _repository;

        public CountAsyncTests()
        {
            var options = new DbContextOptionsBuilder<CasContext>()
                .UseInMemoryDatabase($"UpdateTests-{Guid.NewGuid()}")
                .Options;

            _dbContext = new CasContext(options);
            _dbContext.Companies.Add(new Companies { Active = false });
            _dbContext.SaveChanges();
            _repository = new DataAccess.Repositories.CompaniesRepository(_dbContext);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
            _repository = null;
        }
        
        [Fact]
        public async void CountZero()
        {
            var result = await _repository.CountAsync(x => x.Active);
            Assert.Equal(0,result);
        }

        [Fact]
        public async void CountOne()
        {
            var result = await _repository.CountAsync(x => !x.Active);
            Assert.Equal(1, result);
        }

        [Fact]
        public async void CountUnfiltered()
        {
            var result = await _repository.CountAsync();
            Assert.Equal(1, result);
        }
    }
}
