﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Domain.Entities;
using Domain.Entities.Contexts;
using Domain.IRepositories;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace DataAccess.Tests.Repositories.CompaniesRepository
{
    [ExcludeFromCodeCoverage]
    public class GetAllByAsyncTests: IDisposable
    {

        private readonly CasContext _dbContext;
        private IGenericRepository<Companies> _repository;

        public GetAllByAsyncTests()
        {
            var options = new DbContextOptionsBuilder<CasContext>()
                .UseInMemoryDatabase($"UpdateTests-{Guid.NewGuid()}")
                .Options;

            _dbContext = new CasContext(options);
            _dbContext.Companies.Add(new Companies { Active = false });
            _dbContext.Companies.Add(new Companies { Active = true });
            _dbContext.SaveChanges();
            _repository = new DataAccess.Repositories.CompaniesRepository(_dbContext);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
            _repository = null;
        }
        

        [Fact]
        public async void GetAllWithoutParameters()
        {
            var result = await _repository.GetAllByAsync();
            Assert.NotNull(result);
        }

        [Fact]
        public async void GetAllWitFilter()
        {
            var result = await _repository.GetAllByAsync(x => x.Active);
            Assert.NotNull(result);
        }

        [Fact]
        public async void GetAllWitOrder()
        {
            var result = await _repository.GetAllByAsync(null, x => x.OrderBy(y => y.Active));
            Assert.False(result?.FirstOrDefault() != null && result.First().Active);
        }

        [Fact]
        public async void GetAllWitOrderDescending()
        {
            var result = await _repository.GetAllByAsync(null, x => x.OrderByDescending(y => y.Active));
            Assert.True(result?.FirstOrDefault() != null && result.First().Active);
        }

        [Fact]
        public async void GetAllPaging()
        {
            var result = await _repository.GetAllByAsync(x => x != null, x => x.OrderByDescending(y => y.Active), 1, 1);
            Assert.Single(result);
        }

        [Fact]
        public async void GetAllPagingTracking()
        {
            var result = await _repository.GetAllByAsync(x => x != null, x => x.OrderByDescending(y => y.Active), 1, 1, true);
            Assert.Single(result);
        }
    }
}
