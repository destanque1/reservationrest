﻿using System;
using System.Diagnostics.CodeAnalysis;
using Domain.Entities;
using Domain.Entities.Contexts;
using Domain.IRepositories;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace DataAccess.Tests.Repositories.InstanceRepository
{
    [ExcludeFromCodeCoverage]
    public class InstanceRepositoryTests : IDisposable
    {
        private readonly CasContext _dbContext;
        private IGenericRepository<Instance> _repository;

        public InstanceRepositoryTests()
        {
            var options = new DbContextOptionsBuilder<CasContext>()
                .UseInMemoryDatabase($"UpdateTests-{Guid.NewGuid()}")
                .Options;

            _dbContext = new CasContext(options);
            _dbContext.Instance.Add(new Instance{ Active = false });
            _dbContext.SaveChanges();
            _repository = new DataAccess.Repositories.InstanceRepository(_dbContext);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
            _repository = null;
        }
        

        [Fact]
        public async void GetQueryInclude()
        {
            var result = await _repository.GetQueryBy(null, null, -1, -1, false, x => x.Disk).FirstOrDefaultAsync();
            Assert.NotNull(result.Disk);
        }
    }
}
