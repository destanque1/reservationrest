﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using CrossCutting.Consts;
using CrossCutting.Exceptions;
using Domain.Entities.ValueObjects;
using Domain.Factories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Services.Attributes
{
    [ExcludeFromCodeCoverage]
    public class ApiValidationFilterAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                context.Result = new BadRequestObjectResult(ApiBadRequestResponse(context.ModelState));
            }

            base.OnResultExecuting(context);
        }

        public override Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
        {
            if (!context.ModelState.IsValid)
            {
                context.Result = new BadRequestObjectResult(ApiBadRequestResponse(context.ModelState));
            }

            return base.OnResultExecutionAsync(context, next);
        }

        private static BaseResponse ApiBadRequestResponse(ModelStateDictionary modelState)
        {
            var errors = modelState.Select(x =>
                new SingleError {Location = x.Key, Message = x.Value.Errors.FirstOrDefault()?.ErrorMessage}).ToList();

            return BaseResponseFactory.Create(ResponseCodes.ValidationError, errors);
        }
    }
}
