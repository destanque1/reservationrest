﻿using System.Threading.Tasks;
using Application.Dtos;
using Application.IServices;
using CrossCutting.Dtos;
using CrossCutting.Filters;
using Domain.Entities.ValueObjects;
using Microsoft.AspNetCore.Mvc;

namespace Services.Controllers
{
    [Route("api/reservations")]
    [Produces("application/json")]
    [ApiController]
    public class ReservationController : ControllerBase
    {
        #region Private (Aplication Services)
        private readonly IReservationApplicationService _reservationApplicationService;

        public ReservationController(IReservationApplicationService reservationApplicationService)
        {
            _reservationApplicationService = reservationApplicationService;
        }
        #endregion
       
        [HttpGet]
        public async Task<ActionResult<ListDto<ReservationDto>>> GetReservationsAsync(int pageIndex = 1, int pageSize = 200 , string order = "", bool ascending = true)
        {
            return await _reservationApplicationService.GetReservationsAsync(pageIndex, pageSize, order, ascending);
        }

        [HttpPost("search")]
        public async Task<ActionResult<ListDto<ReservationDto>>> SearchReservationsAsync(SearchParam options)
        {
            return await _reservationApplicationService.SearchReservationsAsync(options);
        }

        [HttpGet("{reservationId:int}")]
        public async Task<ActionResult<ReservationDto>> GetReservationAsync(int reservationId)
        {
            return await _reservationApplicationService.GetReservationAsync(reservationId);
        }

        [HttpPost]
        public async Task<ActionResult<ReservationModel>> CreateReservationAsync(ReservationModel reservationModel)
        {
            return await _reservationApplicationService.CreateReservationAsync(reservationModel);
        }

        [HttpPut("{reservationId:int}")]
        public async Task<ActionResult<ReservationModel>> UpdateReservationAsync(int reservationId, ReservationModel reservationModel)
        {
            return await _reservationApplicationService.UpdateReservationAsync(reservationId, reservationModel);
        }

        [HttpDelete("{reservationId:int}")]
        public async Task<ActionResult<bool>> DeleteReservationAsync(int reservationId)
        {
            return await _reservationApplicationService.DeleteReservationAsync(reservationId);
        }

    }
}
