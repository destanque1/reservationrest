﻿using Microsoft.AspNetCore.Mvc;

namespace Services.Controllers
{
    [Route("api/status")]
    [ApiController]
    public class ApiStatusController : ControllerBase
    {
        [HttpGet]
        public string Status()
        {
            return "Online";
        }
    }
}
