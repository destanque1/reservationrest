﻿using System.Threading.Tasks;
using Application.Dtos;
using Application.IServices;
using CrossCutting.Dtos;
using CrossCutting.Filters;
using Domain.Entities.ValueObjects;
using Microsoft.AspNetCore.Mvc;

namespace Services.Controllers
{
    [Route("api/contactTypes")]
    [Produces("application/json")]
    [ApiController]
    public class ContactTypeController : ControllerBase
    {
        #region Private (Aplication Services)
        private readonly IContactTypeApplicationService _contactTypeApplicationService;

        public ContactTypeController(IContactTypeApplicationService contactTypeApplicationService)
        {
            _contactTypeApplicationService = contactTypeApplicationService;
        }
        #endregion

        
        [HttpGet]   
        public async Task<ActionResult<ListDto<ContactTypeDto>>> GetContactTypesAsync(int pageIndex = 1, int pageSize = 200, string order = "", bool ascending = true)
        {
            return await _contactTypeApplicationService.GetContactTypesAsync( pageIndex, pageSize, order, ascending);
        }


        [HttpPost("search")]
        public async Task<ActionResult<ListDto<ContactTypeDto>>> SearchContactTypesAsync(SearchParam options)
        {
            return await _contactTypeApplicationService.SearchContactTypesAsync(options);
        }

        [HttpGet("{contactTypeId:int}")]
        public async Task<ActionResult<ContactTypeDto>> GetContactTypeAsync(int contactTypeId)
        {
            return await _contactTypeApplicationService.GetContactTypeAsync(contactTypeId);
        }

        [HttpPost]
        public async Task<ActionResult<ContactTypeModel>> CreateContactTypeAsync(ContactTypeModel contactTypeModel)
        {
            return await _contactTypeApplicationService.CreateContactTypeAsync(contactTypeModel);
        }

        [HttpPut("{contactTypeId:int}")]
        public async Task<ActionResult<ContactTypeModel>> UpdateContactTypeAsync(int contactTypeId, ContactTypeModel contactTypeModel)
        {
            return await _contactTypeApplicationService.UpdateContactTypeAsync(contactTypeId, contactTypeModel);
        }

        [HttpDelete("{contactTypeId:int}")]
        public async Task<ActionResult<bool>> DeleteContactTypeAsync(int contactTypeId)
        {
            return await _contactTypeApplicationService.DeleteContactTypeAsync(contactTypeId);
        }
    }
}
