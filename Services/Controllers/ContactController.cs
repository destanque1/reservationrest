﻿using System.Threading.Tasks;
using Application.Dtos;
using Application.IServices;
using CrossCutting.Dtos;
using CrossCutting.Filters;
using Domain.Entities.ValueObjects;
using Microsoft.AspNetCore.Mvc;

namespace Services.Controllers
{
    [Route("api/contacts")]
    [Produces("application/json")]
    [ApiController]
   
    public class ContactController : ControllerBase
    {
        #region Private (Aplication Services)
        private readonly IContactApplicationService _contactApplicationService;

        public ContactController(IContactApplicationService contactApplicationService)
        {
            _contactApplicationService = contactApplicationService;
        }
        #endregion


        [HttpGet]
        public async Task<ActionResult<ListDto<ContactDto>>> GetContactsAsync(int pageIndex = 1, int pageSize = 200, string order = "", bool ascending = true)
        {
            return await _contactApplicationService.GetContactsAsync(pageIndex, pageSize, order, ascending);
        }

        [HttpPost("search")]
        public async Task<ActionResult<ListDto<ContactDto>>> SearchContactsAsync(SearchParam options)
        {
            return await _contactApplicationService.SearchContactsAsync(options);
        }

        [HttpGet("{contactId:int}")]
        public async Task<ActionResult<ContactDto>> GetContactAsync(int contactId)
        {
            return await _contactApplicationService.GetContactAsync(contactId);
        }

        [HttpPost]
        public async Task<ActionResult<ContactModel>> CreateContactAsync(ContactModel contactModel)
        {
            return await _contactApplicationService.CreateContactAsync(contactModel);
        }

        [HttpPut("{contactId:int}")]
        public async Task<ActionResult<ContactModel>> UpdateContactAsync(int contactId, ContactModel contactModel)
        {
            return await _contactApplicationService.UpdateContactAsync(contactId, contactModel);
        }

        [HttpDelete("{contactId:int}")]
        public async Task<ActionResult<bool>> DeleteContactAsync(int contactId)
        {
            return await _contactApplicationService.DeleteContactAsync(contactId);
        }

    }
}
