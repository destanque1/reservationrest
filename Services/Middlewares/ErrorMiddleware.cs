﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CrossCutting.Exceptions;
using Domain.Entities.ValueObjects;
using Domain.Factories;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Services.Middlewares
{
    [ExcludeFromCodeCoverage]
    public class ErrorMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IConfiguration _configs;

        public ErrorMiddleware(RequestDelegate next, IConfiguration configs)
        {
            _next = next;
            _configs = configs;
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            var appName = _configs.GetValue<string>("Application:Name");
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex, appName);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception, string applicationName)
        {
            var code = HttpStatusCode.InternalServerError; // 500 if unexpected

            BaseResponse response;
            if (exception is CustomException ex)
            {
                code = ex.StatusCode;
                response = BaseResponseFactory.Create(ex.Code, ex.Error);
            }
            else if (exception is ValidationException fex)
            {
                code = HttpStatusCode.BadRequest;
                var errors = fex.Errors.Select(error => new SingleError {Location = error.PropertyName, Message = error.ErrorMessage}).ToList();
                response = BaseResponseFactory.Create((int)code, errors);
            }
            else
            {
                response = BaseResponseFactory.Create((int) code, exception.Message, applicationName);
            }

            var result = JsonConvert.SerializeObject(response);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int) code;
            return context.Response.WriteAsync(result);
        }
    }
}
