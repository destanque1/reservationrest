﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO.Compression;
using System.Net.Http;
using Application.IServices;
using Application.Jobs;
using Application.Services;
using DataAccess.Repositories;
using Domain.Entities.Contexts;
using Domain.IRepositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Polly;
using Polly.Extensions.Http;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using Services.Attributes;
using Services.Middlewares;


namespace Services
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        private IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .WaitAndRetryAsync(Configuration.GetValue<int>("Application:HttpClientFactory:MaxConnectionRetry"), retryAttempt =>
                    TimeSpan.FromSeconds(Math.Pow(Configuration.GetValue<int>("Application:HttpClientFactory:MaxConnectionDelay"),
                        retryAttempt)));
        }

        private IAsyncPolicy<HttpResponseMessage> GetCircuitBreakerPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .CircuitBreakerAsync(Configuration.GetValue<int>("Application:HttpClientFactory:MaxCircuitBreakerRetry"), TimeSpan.FromSeconds(
                    Configuration.GetValue<int>("Application:HttpClientFactory:MaxCircuitBreakerDelay")));
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //Jobs
            services.AddSingleton<IJobFactory, JobFactory>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();

            services.AddSingleton<UpdateReservationStatusJob>();
            //It should be run on a once-a-day schedule
            //Run every 20 seconds only for test
            services.AddSingleton(new JobSchedule(jobType: typeof(UpdateReservationStatusJob), cronExpression: "0/20 * * * * ?")); 
            services.AddHostedService<QuartzHostedService>();

            services.AddResponseCompression();

            services.Configure<GzipCompressionProviderOptions>(options =>
            {
                options.Level = CompressionLevel.Fastest;
            });

            //DataAccess layer
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddDbContext<SolutionContext>(o =>
            {
                o.UseSqlServer(Configuration.GetConnectionString("Database"), sqlOptions =>
                {
                    sqlOptions.EnableRetryOnFailure(10, TimeSpan.FromSeconds(30), null);
                });
            });

            //Application layer
            services.AddScoped<IReservationApplicationService, ReservationApplicationService>();
            services.AddScoped<IContactTypeApplicationService, ContactTypeApplicationService>();
            services.AddScoped<IContactApplicationService, ContactApplicationService>();

            //Register global filters
            services.AddMvc(options =>
            {
                options.Filters.Add(new ApiValidationFilterAttribute());
            });

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        );
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (!env.IsDevelopment())
            {
                app.UseHsts();
            }

            app.UseMiddleware(typeof(ErrorMiddleware));
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseResponseCompression();
            app.UseCors("CorsPolicy");
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            
        }
    }
}
