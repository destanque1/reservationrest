﻿using Domain.Entities;
using Domain.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
    public class ReservationStatusRepository : GenericRepository<ReservationStatus>, IReservationStatusRepository
    {
        public ReservationStatusRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
