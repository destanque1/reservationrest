﻿using Domain.Entities;
using Domain.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
    public class ReservationRepository : GenericRepository<Reservation>, IReservationRepository
    {
        public ReservationRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
