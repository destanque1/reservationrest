﻿using Domain.Entities;
using Domain.IRepositories;
using Microsoft.EntityFrameworkCore;


namespace DataAccess.Repositories
{
    public class ContactTypeRepository : GenericRepository<ContactType>, IContactTypeRepository
    {
        public ContactTypeRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
