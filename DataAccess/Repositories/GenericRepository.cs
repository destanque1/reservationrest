﻿#region Namespaces

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Domain.IRepositories;
using Microsoft.EntityFrameworkCore;

#endregion Namespaces

namespace DataAccess.Repositories
{
    
    public abstract class GenericRepository<TEntity> : IGenericRepository<TEntity>
        where TEntity : class
    {
        private readonly DbContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public GenericRepository(DbContext dbContext)
        {
            _context = dbContext;
            _dbSet = _context.Set<TEntity>();
        }

        public void Create(TEntity entity)
        {
            _dbSet.Add(entity);
        }

        public void Update(TEntity entity)
        {
            _dbSet.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(object id)
        {
            var entityToDelete = _dbSet.Find(id);
            Delete(entityToDelete);
        }

        public void Delete(TEntity entity)
        {
            if (_context.Entry(entity).State == EntityState.Detached)
            {
                _dbSet.Attach(entity);
            }
            _dbSet.Remove(entity);
        }

        public async Task DeleteAsync(Expression<Func<TEntity, bool>> filter)
        {
            var objects = await GetAllByAsync(filter, tracking: true);
            foreach (var item in objects)
            {
                Delete(item);
            }
        }

        public IQueryable<TEntity> GetQueryBy(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> sortFilter = null, int pageIndex = -1, int pageSize = -1,
            bool tracking = false, params Expression<Func<TEntity, object>>[] include)
        {
            IQueryable<TEntity> source = _dbSet;

            if (filter != null)
            {
                source = source.Where(filter);
            }
            source = include != null && include.Any() ? include.Aggregate(source, (a, b) => a.Include(b)) : source;

            if (sortFilter != null)
            {
                source = sortFilter(source);
            }

            if (pageIndex > 0 && pageSize > 0)
                source = source.Skip(pageIndex * pageSize - pageSize).Take(pageSize);

            return tracking ? source : source.AsNoTracking();
        }

        public async Task<TEntity> GetOneByAsync(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> sort = null, bool tracking = false,
            params Expression<Func<TEntity, object>>[] include)
        {
           return await GetQueryBy(filter, sort, tracking: tracking, include: include).FirstOrDefaultAsync();
        }

        public async Task<List<TEntity>> GetAllByAsync(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> sort = null, int pageIndex = -1, int pageSize = -1,
            bool tracking = false, params Expression<Func<TEntity, object>>[] include)
        {
            return await GetQueryBy(filter, sort, pageIndex, pageSize, tracking, include)
                .ToListAsync();
        }

        public async Task<int> CountAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            return await GetQueryBy(filter).CountAsync();
        }

        public async Task<bool> AnyAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            return filter == null ? await _dbSet.AnyAsync() : await _dbSet.AnyAsync(filter);
        }

        public async Task<int> ExecStoreProcedure(string query, params object[] parameters)
        {
           return await _context.Database.ExecuteSqlRawAsync(query, parameters);
        }
    }
}