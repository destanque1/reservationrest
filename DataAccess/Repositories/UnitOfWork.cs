﻿using System;
using System.Threading.Tasks;
using Domain.Entities.Contexts;
using Domain.IRepositories;

namespace DataAccess.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool _disposed;
        private readonly SolutionContext _context;
        
        private IContactRepository _contactRepository;
        private IContactTypeRepository _contactTypeRepository;
        private IReservationRepository _reservationRepository;
        private IReservationStatusRepository _reservationStatusRepository;

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public IContactRepository ContactRepository =>
            _contactRepository ??= new ContactRepository(_context);

        public IContactTypeRepository ContactTypeRepository =>
            _contactTypeRepository ??= new ContactTypeRepository(_context);

        public IReservationRepository ReservationRepository =>
            _reservationRepository ??= new ReservationRepository(_context);

        public IReservationStatusRepository ReservationStatusRepository =>
            _reservationStatusRepository ??= new ReservationStatusRepository(_context);

      

        public UnitOfWork(SolutionContext context)
        {
            _disposed = false;
            _context = context;
        }

        public async Task<int> CommitAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
