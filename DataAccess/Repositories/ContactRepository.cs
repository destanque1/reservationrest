﻿using Domain.Entities;
using Domain.IRepositories;
using Microsoft.EntityFrameworkCore;


namespace DataAccess.Repositories
{
    public class ContactRepository : GenericRepository<Contact>, IContactRepository
    {
        public ContactRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
