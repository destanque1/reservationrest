﻿using System.Collections.Generic;
using CrossCutting.Exceptions;
using Domain.Entities.ValueObjects;

namespace Domain.Factories
{
    public static class GenericResponseFactory<T>
    {
        public static GenericResponse<T> Create(int code, T value)
        {
            return new GenericResponse<T>
            {
                Code = code,
                Value = value
            };
        }

        public static GenericResponse<T> Create(int code, string errorMessage, T value)
        {
            return new GenericResponse<T>
            {
                Code = code,
                Errors = new List<SingleError> {new SingleError
                {
                    Location = "Message",
                    Message = errorMessage
                }},
                Value = value
            };
        }

        public static GenericResponse<T> Create(int code, SingleError error, T value)
        {
            return new GenericResponse<T>
            {
                Code = code,
                Errors = new List<SingleError> {error},
                Value = value
            };
        }

        public static GenericResponse<T> Create(int code, List<SingleError> errors, T value)
        {
            return new GenericResponse<T>
            {
                Code = code,
                Errors = errors,
                Value = value
            };
        }
    }
}
