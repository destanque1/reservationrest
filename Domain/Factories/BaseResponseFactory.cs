﻿using System.Collections.Generic;
using CrossCutting.Exceptions;
using Domain.Entities.ValueObjects;

namespace Domain.Factories
{
    public static class BaseResponseFactory
    {
        public static BaseResponse Create(int code)
        {
            return new BaseResponse
            {
                Code = code,
                Errors = new List<SingleError>()
            };
        }

        public static BaseResponse Create(int code, string errorMessage, string location = "ApiServices")
        {
            return new BaseResponse
            {
                Code = code,
                Errors = new List<SingleError>
                {
                    new SingleError
                    {
                        Location = location,
                        Message = errorMessage
                    }
                }
            };
        }

        public static BaseResponse Create(int code, SingleError error)
        {
            return new BaseResponse
            {
                Code = code,
                Errors = new List<SingleError> {error}
            };
        }

        public static BaseResponse Create(int code, List<SingleError> errors)
        {
            return new BaseResponse
            {
                Code = code,
                Errors = errors
            };
        }
    }
}
