﻿

using Domain.Entities;

namespace Domain.IRepositories
{
    public interface IContactRepository : IGenericRepository<Contact>
    {
    }
}
