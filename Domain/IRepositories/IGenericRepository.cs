﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Domain.IRepositories
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Get query
        /// Disabling change tracking is useful for read-only scenarios because it avoids
        /// the overhead of setting up change tracking for each entity instance. You should
        /// not disable change tracking if you want to manipulate entity instances and persist
        /// those changes to the database using Microsoft.EntityFrameworkCore.DbContext.SaveChanges.
        /// Identity resolution will still be performed to ensure that all occurrences of
        /// an entity with a given key in the result set are represented by the same entity
        /// </summary>
        /// <param name="filter">Filer</param>
        /// <param name="sortFilter"></param>
        /// <param name="pageIndex">Elements to ignore</param>
        /// <param name="pageSize">Elements to take</param>
        /// <param name="tracking"></param>
        /// <param name="include"></param>
        /// <returns>Returns all entities that matches all conditions</returns>
        IQueryable<TEntity> GetQueryBy(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> sortFilter = null, int pageIndex = -1,
            int pageSize = -1, bool tracking = false,
            params Expression<Func<TEntity, object>>[] include);

        /// <summary>
        /// Get async one elements or null
        /// Disabling change tracking is useful for read-only scenarios because it avoids
        /// the overhead of setting up change tracking for each entity instance. You should
        /// not disable change tracking if you want to manipulate entity instances and persist
        /// those changes to the database using Microsoft.EntityFrameworkCore.DbContext.SaveChanges.
        /// Identity resolution will still be performed to ensure that all occurrences of
        /// an entity with a given key in the result set are represented by the same entity
        /// instance.
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="tracking"></param>
        /// <param name="sort"></param>
        /// <param name="include"></param>
        /// <returns>
        /// Returns one entity that matches all conditions or null
        /// </returns>
        Task<TEntity> GetOneByAsync(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> sort = null, bool tracking = false, params Expression<Func<TEntity, object>>[] include);

        /// <summary>
        /// Get all elements
        /// Disabling change tracking is useful for read-only scenarios because it avoids
        /// the overhead of setting up change tracking for each entity instance. You should
        /// not disable change tracking if you want to manipulate entity instances and persist
        /// those changes to the database using Microsoft.EntityFrameworkCore.DbContext.SaveChanges.
        /// Identity resolution will still be performed to ensure that all occurrences of
        /// an entity with a given key in the result set are represented by the same entity
        /// </summary>
        /// <param name="filter">Filer</param>
        /// <param name="sort">Order by</param>
        /// <param name="pageIndex">Elements to ignore</param>
        /// <param name="pageSize">Elements to take</param>
        /// <param name="tracking"></param>
        /// <param name="include"></param>
        /// <returns>Returns all entities that matches all conditions</returns>
        Task<List<TEntity>> GetAllByAsync(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> sort = null, int pageIndex = -1,
            int pageSize = -1, bool tracking = false, params Expression<Func<TEntity, object>>[] include);


        /// <summary>
        /// Count of elem that match a condition
        /// </summary>
        /// <param name="filter">Condition</param>
        /// <returns>Element</returns>
        Task<int> CountAsync(Expression<Func<TEntity, bool>> filter = null);

        /// <summary>
        /// Check if exist any result, accourding to the where condition
        /// </summary>
        /// <param name="filter"></param>
        /// <returns>Boolean, indicate if exist any result</returns>
        Task<bool> AnyAsync(Expression<Func<TEntity, bool>> filter = null);

        /// <summary>
        /// Creates the existing entity.
        /// </summary>
        void Create(TEntity entity);

        /// <summary>
        /// Updates the existing entity.
        /// </summary>
        void Update(TEntity entity);

        /// <summary>
        /// Delete an entity using its primary key.
        /// </summary>
        /// <param name="id"></param>
        void Delete(object id);

        /// <summary>
        /// Delete the given entity.
        /// </summary>
        /// <param name="entity"></param>
        void Delete(TEntity entity);

        /// <summary>
        /// Deletes the existing entity.
        /// </summary>
        /// <param name="filter"></param>
        Task DeleteAsync(Expression<Func<TEntity, bool>> filter = null);

        /// <summary>
        /// Exec Store Procedure
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task<int> ExecStoreProcedure(string query, params object[] parameters);
    }
}
