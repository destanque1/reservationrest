﻿using Domain.Entities;


namespace Domain.IRepositories
{
    public interface IContactTypeRepository : IGenericRepository<ContactType>
    {
    }
}
