﻿using Domain.Entities;

namespace Domain.IRepositories
{
    public interface IReservationStatusRepository : IGenericRepository<ReservationStatus>
    {
    }
}
