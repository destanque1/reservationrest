﻿using System;
using System.Threading.Tasks;

namespace Domain.IRepositories
{
    public interface IUnitOfWork : IDisposable
    {
        IContactRepository ContactRepository { get; }
        IContactTypeRepository ContactTypeRepository { get; }
        IReservationRepository ReservationRepository { get; }
        IReservationStatusRepository ReservationStatusRepository { get; }
        Task<int> CommitAsync();
    }
}
