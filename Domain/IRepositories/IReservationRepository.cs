﻿using Domain.Entities;

namespace Domain.IRepositories
{
    public interface IReservationRepository : IGenericRepository<Reservation>
    {
    }
}
