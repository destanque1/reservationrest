﻿using System.Runtime.Serialization;

namespace Application.Dtos
{
    [DataContract]
    public class ContactTypeDto
    {
        [DataMember] public int ContactTypeId { get; set; }
        [DataMember] public string Code { get; set; }
        [DataMember] public string Description { get; set; }

    }
}
