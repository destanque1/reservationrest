﻿using System;
using System.Runtime.Serialization;


namespace Application.Dtos
{
    [DataContract]
    public class ReservationDto
    {
        [DataMember] public int ReservationId { get; set; }

        [DataMember] public string Description { get; set; }

        [DataMember] public int Ranking { get; set; }

        [DataMember] public DateTime Date { get; set; }

        [DataMember] public string Contact { get; set; }
        [DataMember] public int ContactId { get; set; }
        [DataMember] public string Status { get; set; }


    }
}
