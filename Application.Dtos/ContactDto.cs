﻿using System;
using System.Runtime.Serialization;

namespace Application.Dtos
{
    [DataContract]
    public class ContactDto
    {
        [DataMember] public int ContactId { get; set; }

        [DataMember] public string Name { get; set; }

        [DataMember] public string Phone { get; set; }

        [DataMember] public string Type { get; set; }
        [DataMember] public DateTime BirthDate { get; set; }

    }
}
