﻿using Domain.Entities;


namespace Application.Dtos.Adapters
{
    public static class ContactTypeDtoAdapter
    {
        public static ContactTypeDto Create(ContactType contactType)
        {
            if (contactType == null) return null;
            return new ContactTypeDto
            {
               ContactTypeId = contactType.ContactTypeId,
               Code = contactType.Code,
               Description = contactType.Description
            };
        }

       
    }
}
