﻿


using Domain.Entities;

namespace Application.Dtos.Adapters
{
    public static class ContactDtoAdapter
    {
        /// <summary>
        /// Responsible for convert entity to Dto
        /// </summary>
        /// <param name="contact"></param>
        /// <returns>Returns ContactDto entity </returns>
        public static ContactDto Create(Contact contact)
        {
            if (contact == null) return null;
            return new ContactDto
            {
               ContactId = contact.ContactId,
               Name = contact.Name,
               Phone = contact.Phone,
               Type = contact.ContactType?.Description ?? string.Empty,
               BirthDate = contact.BirthDate
            };
        }

    }
}
