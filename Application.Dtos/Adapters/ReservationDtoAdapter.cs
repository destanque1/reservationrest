﻿using Domain.Entities;

namespace Application.Dtos.Adapters
{
    public static class ReservationDtoAdapter
    {
        public static ReservationDto Create(Reservation reservation)
        {
            if (reservation == null) return null;
            return new ReservationDto
            {
               ReservationId = reservation.ReservationId,
               Description = reservation.Description,
               Contact = reservation.Contact?.Name ?? string.Empty,
               Status = reservation.ReservationStatus?.Description ?? string.Empty,
               Date = reservation.Date,
               Ranking = reservation.Ranking,
               ContactId = reservation.Contact?.ContactId ?? 0

            };
        }

       
    }
}
